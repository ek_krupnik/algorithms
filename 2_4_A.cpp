#include <iostream>
#include <vector>

using namespace std;

const int INF = numeric_limits<int>::max();

class SparseTables {

public:

	explicit SparseTables (int size, vector<int> array);
	pair<int, int> Query (int a, int b) const;

private:

	int size;
	vector<vector<pair<int, int>>> table;
	vector<int> degrees;
};

vector<int> PrecompileDegree (int n) {

	vector<int> res (n + 1, 0);
	for (int i = 2; i <= n; i++) {
		res[i] = res[i / 2] + 1;
	}

	return res;
}

pair<int, int> FindMin (pair<int, int> a, pair<int, int> b) {

	int first_min  = 0;
	int second = 0;

	if (a.first < b.first) {
		first_min  = a.first;
		second = a.second;
	} else {
		first_min = b.first;
		second = b.second;
	}

	return {first_min, second};
}

SparseTables::SparseTables (int new_size, vector<int> array) {

	size = new_size;
	table.resize(new_size + 2);
	for (int ind = 0; ind < array.size(); ind++) {
		table[ind].push_back({array[ind], ind});
	}

	degrees = PrecompileDegree (size);

	for (int j = 1; (1 << j) <= size; j++) {
		for (int i = 0; i + (1 << j) - 1 <= size; i++) {
			table[i].push_back ({0, 0});
		}
	}

	for (int j = 1; (1 << j) + 1 <= size; j++) {
		for (int i = 0; i + (1 << j) <= size; i++) {
			table[i][j] = FindMin (table[i][j - 1], table[i + (1 << (j - 1))][j - 1]);
		}
	}
}

pair<int, int> SparseTables::Query (int a, int b) const {

	if (b < a) {
		return {INF, INF};
	}

	int ind = degrees[b - a + 1];
	int i = b - (1 << ind) + 1;

	if (i < 0 || i >= size) {
		return {INF, INF};
	}

	return (FindMin(table[a][ind], table[i][ind]));
}

int FindSecondMin (const SparseTables& table, int a, int b) {

	int ind = table.Query(a, b).second;
	return min (table.Query(a, ind - 1).first, table.Query(ind + 1, b).first);
}

int main() {

	int n, m;
	cin >> n >> m;

	vector<int> array;

	for (int i = 0; i < n; i++) {
		int a;
		cin >> a;
		array.push_back(a);
	}

	SparseTables table(n + 1, array);

	for (int i = 0; i < m; i++) {
		int a, b;
		cin >> a >> b;
		cout << FindSecondMin (table, a - 1, b - 1) << endl;
	}
}