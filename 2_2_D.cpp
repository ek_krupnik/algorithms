/*
	Рик отправляет Морти в путешествие по N вселенным. У него есть список всех существующих однонаправленных 
	телепортов. Чтобы Морти не потерялся, Рику необходимо узнать, между какими вселенными существуют пути, а 
	между какими нет. Помогите ему в этом!
*/

#include <iostream>
#include <vector>
#include <string>
#include <bitset>

using namespace std;

const int SIZE = 1000;

class Graph {

public:

	Graph (int n) : size (n) {}

	void AddEdge (int data, int from, int to);
	int GetSize () const;

	friend vector<vector<int>> FindAllLinks (Graph& graph);
	// in order to have fast access to bitset

private:

	int size = 0;
	bitset<SIZE> bit_mask[SIZE];
	friend void UpdateLinks (Graph& graph);
};

int Graph::GetSize () const {

	return size;
}

void Graph::AddEdge (int data, int from, int to) {

	if (data) {
		bit_mask[from][to] = true;
	} else {
		bit_mask[from][to] = false;
	}
}


void UpdateLinks (Graph& graph) {

	for (int k = 0; k < graph.GetSize(); k++) {
		for (int i = 0; i < graph.GetSize(); i++) {

			if (graph.bit_mask[i][k]) {
				graph.bit_mask[i] |= graph.bit_mask[k];
			}
		}
	}
}

vector<vector<int>> FindAllLinks (Graph& graph) {

	UpdateLinks (graph);
	vector<vector<int>> result(graph.GetSize());

	for (int i = 0; i < graph.GetSize(); i++) {
		for (int j = 0; j < graph.GetSize(); j++) {

				if (graph.bit_mask[i][j]) {
					result[i].push_back(1);
				} else {
					result[i].push_back(0);
				}
		}
	}

	return result;
}

int main() {
	
	int n;
	cin >> n;

	Graph graph(n);

	for (int i = 0; i < n; i++) {

		string data;
		cin >> data;

		for (int j = 0; j < n; j++) {

			graph.AddEdge ((data[j] - '0'), i, j);
		}
	}

	for (auto i : FindAllLinks(graph)) {
		for (auto j : i) {
			cout << j;
		}
		cout << endl;
	}
}