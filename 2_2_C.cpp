/*
	Сейчас Рику надо попасть из вселенной с номером S во вселенную с номером F. Он знает все существующие телепорты, и казалось бы нет никакой проблемы. 
	Но, далеко не секрет, что за свою долгую жизнь Рик поссорился много с кем. Из своего личного опыта он знает, что при телепортациях есть вероятность, 
	что его заставят ответить за свои слова.
	Если Рик знает вероятности быть прижатым к стенке на всех существующих телепортациях, помогите ему посчитать минимальную вероятность, что он всё-таки 
	столкнется с неприятностями.
*/
#include <iostream>
#include <vector>
#include <set>

const int INF = 101;

using namespace std;

struct Node {

	long double len;
	long long to;

	Node (long double n_len, long long n_to) : len(n_len), to(n_to) {}
};

class Graph {

public:

	Graph (long long new_size = 0) : links(new_size), size(new_size) {}

	void Add (long long from, long long to, long double data);
	long long GetSize () const {return size;}
	vector<Node> GetLinks (long long v) const {return links[v];}

private:

	vector<vector<Node>> links;
	long long size = 0;

};

void Graph::Add (long long from, long long to, long double data) {

	links[from].push_back(Node(data, to));
}

long double LongestWay (const Graph& graph, long long start, long long end) {

	vector<long double> dist (graph.GetSize(), -INF);
	dist[start] = 1;

	set<pair<long double, long long>> edges; 				
	edges.insert ({- dist[start], start});

	while (!edges.empty()) {

		long long vert = edges.begin() -> second; 			
		edges.erase (edges.begin()); 						

		for (auto i : graph.GetLinks(vert)) {
			
			if (dist[vert] * i.len > dist[i.to]) {   		
				edges.erase  ({- dist[i.to], i.to});
				dist[i.to] = dist[vert] * i.len;
				edges.insert ({- dist[i.to], i.to});
			}

		}
	}

	return dist[end];
}

int main() {

	long long n = 0;
	long long m = 0;
	cin >> n >> m;

	Graph graph(n);

	long long start = 0;
	long long end = 0;
	cin >> start >> end;

	for (long long i = 0; i < m; i++) {

		long long from, to;
		long double data;
		cin >> from >> to >> data;

		graph.Add (from - 1, to - 1, (100.0 - data) / 100);
		graph.Add (to - 1, from - 1, (100.0 - data) / 100);	
	}

	long double answer = 1.0 - LongestWay (graph, start - 1, end - 1);
	cout << (answer < 1.0 ? answer : 1.0) << endl;
}