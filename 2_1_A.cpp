#include <iostream>
#include <vector>
#include <queue>
#include <climits>

using namespace std;

class Graph {

public:

	Graph (int new_size = 0) : links(new_size), size(new_size) {}

	void Add (int from, int to);
	int GetSize () const;
	vector<int> GetNeigbours (int v) const;

private:

	int size;
	vector<vector<int>> links;
};

int Graph::GetSize () const {

	return size;
}

vector<int> Graph::GetNeigbours (int v) const {
	return links[v];
}

void BFS (const Graph& graph, int start, vector<int>& dist) {

	queue<int> vertices;
 	vertices.push(start);

	dist[start] = 0;

	while (!vertices.empty()) {

		int v = vertices.front();
		vertices.pop();

		for (auto i : graph.GetNeigbours(v)) {
			if (dist[i] == -1) {
				vertices.push(i);
				dist[i] = dist[v] + 1;
			}
		}
	}
}

int FindAnswer (const int& leon, const int& matilda, const int& milk, const Graph& graph) {

	vector<vector<int>> dist(3, vector<int>(graph.GetSize(), -1));

	BFS(graph, leon, dist[0]);
	BFS(graph, matilda, dist[1]);
	BFS(graph, milk, dist[2]);

	int answer = INT_MAX;
	// it's from <climits> 

	for (int i = 0; i < graph.GetSize(); i++) {
		if (dist[0][i] == -1 || dist[1][i] == -1 || dist[2][i] == -1) {
			continue;
		}
		int local_ans = dist[0][i] + dist[1][i] + dist[2][i];
		answer = min(answer, local_ans);
	}

	return answer;
}

void Graph::Add (int from, int to) {

	links[from].push_back(to);
	links[to].push_back(from);
}

int main() {
	
	int n, m;
	cin >> n >> m;

	Graph graph = Graph(n);

	int leon, matilda, milk;
	cin >> leon >> matilda >> milk;

	for (int i = 0; i < m; i++) {
		int from, to;
		cin >> from >> to;
		graph.Add(from - 1, to - 1);
	}

	cout << FindAnswer(leon - 1, matilda - 1, milk - 1, graph);
}