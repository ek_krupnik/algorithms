#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

struct Data {

	int in;
	int out;

	Data (int new_in = 0, int new_out = 0) : in(new_in), out(new_out) {} 
};

bool Cmp (Data first, Data second) {

	if (first.out < second.out) {
		return true;
	}
	else if (first.out == second.out && first.in < second.in) {
		return true;
	}

	return false;
}

int FindMaxCount (vector<Data> time) {

	sort(time.begin(), time.end(), Cmp);

	int answer = 1;
	int ind = 1;
	int end = time[0].out;
	while (ind < time.size()) {
		while (time[ind].in < end && ind < time.size()) {
			ind++;
		}

		if (ind == time.size()) {
			return answer;
		}

		end = time[ind].out;
		answer ++;
	}

	return answer;
}	

int main() {

	vector<Data> time;
	int time_in = 0;
	int time_out = 0;

	while(cin >> time_in >> time_out) {
		time.push_back(Data (time_in, time_out));
	} 
	
	cout << FindMaxCount (time);
}