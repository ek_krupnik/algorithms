#include <iostream>
#include <vector>
#include <stack>

using namespace std;

class Graph {

public: 

	Graph (int new_size = 0) : links(new_size), size(new_size) {}

	void Add (int from, int to);
	int GetSize () const {return size; };
	vector<int> GetLinks (int v) const {return links[v]; }

private:

	int size;
	vector<vector<int>> links;
};

enum Using{
	not_used, 
	is_using, 
	was_used,
};

void Graph::Add (int from, int to) {

	links[from].push_back(to);
}

void Print (vector<int> &arr) {

	for (auto i : arr) {
		cout << i << ' ';
	}
	cout << endl;
}

bool IsCycled (int v, const Graph& graph, vector<Using> &used, vector<int>& in_top_sort) {

	used[v] = is_using;
	in_top_sort.push_back(v);

	for (auto to : graph.GetLinks(v)) {

		if (used[to] == is_using) {
			return true;
		} 
		if (used[to] == not_used) {
			IsCycled (to, graph, used, in_top_sort);
		}
	}

	used[v] = was_used;
	return false;
}

bool FindCycles (const Graph& graph, vector<int>& in_top_sort) {

	vector<Using> used(graph.GetSize(), not_used);
	for (int i = 0; i < graph.GetSize(); i++) {
		if (used[i] == not_used && IsCycled(i, graph, used, in_top_sort)) {
			return true;
		}
	}

	return false;
}

pair<bool, vector<int>> FindAnswer (const Graph& graph) {
	
	vector<int> in_top_sort;
	bool res;

	if (!FindCycles(graph, in_top_sort)) {
		res = true;

	} else {
		res = false;
	}

	return {res, in_top_sort};
}

int main() {

	int n, m;
	cin >> n >> m;

	Graph graph = Graph(n);
	for (int i = 0; i < m; i++) {
		int from, to;
		cin >> from >> to;
		graph.Add(from, to);
	}
	
	pair<bool, vector<int>> answer = FindAnswer(graph);

	if (answer.first) {
		cout << "YES\n";
		Print(answer.second);

	} else {
		cout << "NO\n";
	}
}