#include <iostream>
#include <vector>
#include <set>

using namespace std;

struct Link {

	long long weight;
	long long to;

	Link (long long weight, long long to) : weight(weight), to(to) {}
};

class Graph {

public:

	Graph (long long new_size = 0) : links(new_size), size(new_size) {}

	void Add (long long from, long long to, long long weight);
	void AddPrice (long long data);

	long long GetSize () const {return size;}
	vector<Link> GetLinks (long long v) const {return links[v];}
	long long GetEdgeWeight (long long from, long long to) const {return price[from] + price[to];}

	long long GetPrice (long long v) const {return price[v];} 

private:

	vector<vector<Link>> links;
	vector<long long> price;
	long long size = 0;

};

void Graph::Add (long long from, long long to, long long weight) {

	links[from].push_back(Link(weight, to));
}

void Graph::AddPrice (long long data) {

	price.push_back(data);
}

long long FindMinOstWeight (const Graph& graph) {

	long long answer = 0;

	// first - weight second - destination vertex
	set<pair<long long, long long>> edges; 				
	vector<bool> in_min_ost (graph.GetSize(), false);
	long long cnt_in_min_ost = 0;

	set<pair<long long, long long>> used;
	set<pair<long long, long long>> unused;

	for (long long i = 0; i < graph.GetSize(); i++) {
		unused.insert({graph.GetPrice(i), i});
	}

	in_min_ost[0] = true;
	cnt_in_min_ost ++;
	used.insert ({graph.GetPrice(0), 0});
	unused.erase (unused.find({graph.GetPrice(0), 0}));

	for (auto edge : graph.GetLinks(0)) {
		edges.insert({edge.weight, edge.to});
	}

	while (cnt_in_min_ost < graph.GetSize()) {

		long long unused_top = unused.begin() -> second;
		long long used_top = used.begin() -> second;

		long long vert;
		if (!edges.empty()){

			vert = edges.begin() -> second;
		
			if (in_min_ost[vert]) {
				edges.erase (edges.begin()); 
				continue;
			}
		}

		if (edges.empty() || graph.GetEdgeWeight(unused_top, used_top) < edges.begin() -> first) {

			answer += graph.GetEdgeWeight(unused_top, used_top);
			vert = unused_top;
		} else {

			answer += edges.begin() -> first;
			edges.erase (edges.begin()); 
		}

		in_min_ost[vert] = true;
		used.insert({graph.GetPrice(vert), vert});
		unused.erase(unused.find({graph.GetPrice(vert), vert}));
		cnt_in_min_ost ++;						

		for (auto edge : graph.GetLinks(vert)) {
			if (!in_min_ost[edge.to]) {
				edges.insert({edge.weight, edge.to});
			}
		}
	}

	return answer;
}

int main() {

	long long n, m;
	cin >> n >> m;

	Graph graph(n);

	for (long long i = 0; i < n; i++) {
		long long a;
		cin >> a;
		graph.AddPrice (a);
	}

	for (long long i = 0; i < m; i++) {

		long long from, to;
		long long weight;

		cin >> from >> to >> weight;
		graph.Add (from - 1, to - 1, weight);
		graph.Add (to - 1, from - 1, weight);
	}

	cout << FindMinOstWeight (graph);
}