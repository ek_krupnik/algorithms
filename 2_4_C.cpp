
#include <iostream>
#include <stack>
#include <vector>
#include <cassert>

using namespace std;

// more than maximum sum of colors
const int MAX_C = 255 * 3 + 1;

struct Node {

    int color;
    int upd;

    int l;
    int r;

    Node* l_child;
    Node* r_child;

    Node () : color(MAX_C), upd(-1), l(0), r(0), l_child(nullptr), r_child(nullptr) {};

    ~Node () {
        delete l_child;
        delete r_child;
    }
};

void Update (Node *node) {
    if (node->upd != -1) {
        node->color = node->upd;
    
        if (node->l != node->r){
            node->l_child->upd = node->upd;
            node->l_child->color = node->upd;

            node->r_child->upd = node->upd;
            node->r_child->color = node->upd;
            node->upd = -1;
            node->color = min (node->l_child->color, node->r_child->color);
        }
    }
  
    if (node->l != node->r) {
        node->color = min (node->l_child->color, node->r_child->color);
    }
}

class SegmentTree {

public:

    SegmentTree (vector<int> &start_state, int start, int end);
    ~SegmentTree () {
        delete root;
    };

    Node* GetRoot () const {return root;}
    void Change (Node *root, int a, int b, int add);
    int MakeQuery (int a, int b, int data, int c, int d);

private:

    Node* Build (vector<int> &data, Node *root, int l, int r);
    int Query (Node *root, int a, int b);
    Node* root;
};

SegmentTree::SegmentTree (vector<int> &start_state, int start, int end) {
    root = Build (start_state, GetRoot(), start, end);
}

void SegmentTree::Change (Node *root, int l, int r, int val) {

    if (root->l > r || root->r < l) {
        return;
    }
    
    if (root->l >= l && root->r <= r) {
        root->upd = val;
        Update (root);
        return;
    }
    
    Change (root->l_child, l, r, val);
    Change (root->r_child, l, r, val);
    
    Update (root);
}

Node* SegmentTree::Build (vector<int> &data, Node *root, int l, int r) {

    root = new Node ();

    if (l == r) {
        root->color = data[l];
    } else {
        
        int m = (l + r) / 2;
        root->l_child = Build (data, root->l_child, l, m);
        root->r_child = Build (data, root->r_child, m + 1, r);
        
        root->color = min (root->l_child->color, root->r_child->color);
    }
    
    root->l = l;
    root->r = r;

    return root;
}

int SegmentTree::Query (Node *root, int l, int r) {

    Update (root);
    
    if (root->l > r || root->r < l) {
        return MAX_C;
    }
    
    if (root->l >= l && root->r <= r) {
        return root->color;
    }

    return min (Query (root->l_child, l, r), Query (root->r_child, l, r));
}

int SegmentTree::MakeQuery (int a, int b, int data, int c, int d) {

    Node* root = GetRoot();
    Change (root, a, b, data);

    return Query (root, c, d);
}

int main() {
    
    int n = 0;
    cin >> n;

    vector<int> start_state (n, 0);

    for (int i = 0; i < n; i++) {
    	int r, g, b;
        cin >> r >> g >> b;
        start_state[i] = r + g + b;
    }

    SegmentTree tree(start_state, 0, n - 1);

    int m = 0;
    cin >> m;
    
    for (int i = 0; i < m; i++) {
        int x, y, r, g, b, c, d;
        cin >> x >> y;
        cin >> r >> g >> b;
        cin >> c >> d;
        cout << tree.MakeQuery (x, y, r + g + b, c, d) << ' ';
    }
}