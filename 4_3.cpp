#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<pair<int64_t, int> > my_vector;

void Print(vector<pair<int64_t, int> > &array){
	for(int i = 0; i < array.size(); i++){
		cout << array[i].first << ' ' << array[i].second << endl;
	}
}

void Merge(my_vector &array, int l, int m, int r, int64_t &answer){

	int size_l = m - l + 1;
	int size_r = r - m;

	my_vector left(size_l);
	my_vector right(size_r);

	for(int i = 0; i < size_l; i++){
		left[i] = array[l + i];
	}
	for(int i = 0; i < size_r; i++){
		right[i] = array[m + 1 + i];
	}

	int i = 0;
	int j = 0;
	int k = l;

	while (i < size_l && j < size_r){

		if (left[i].first > right[j].first){
			answer += size_r - j;
			array[k ++] = left[i ++];
		}
		else{
			array[k ++] = right[j ++];
		}
	}

	while (i < size_l){
		array[k ++] = left[i ++];
	}

	while (j < size_r){
		array[k ++] = right[j ++];
	}
}

void MergeSort(my_vector &array, int l, int r, int64_t &answer){

	if (l == r){
		return;
	}

	int m = (l + r) / 2;

	MergeSort(array, l, m, answer);
	MergeSort(array, m + 1, r, answer);

	Merge(array, l, m, r, answer);
}

int main(){

	int64_t a;
	vector<pair<int64_t, int> > array;
	int ind = 0;
	int64_t answer = 0;

	while (cin >> a){
		array.push_back(make_pair(a, ind ++));
	}

	MergeSort(array, 0, array.size() - 1, answer);
	cout << answer;

}