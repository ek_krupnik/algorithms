/*
	Рик и Морти снова бороздят просторы вселенных, но решили ограничиться только теми, номера которых меньше M. 
	Они могут телепортироваться из вселенной с номером z во вселенную (z+1) mod M за a бутылок лимонада или во вселенную 
	(z2+1) mod M за b бутылок лимонада. Рик и Морти хотят добраться из вселенной с номером x во вселенную с номером y. 
	Сколько бутылок лимонада отдаст Рик за такое путешествие, если он хочет потратить их как можно меньше?
*/
#include <iostream>
#include <vector>
#include <set>

using namespace std;

struct Link {

	long long to;
	long long weight;

	Link (long long to, long long weight) : to(to), weight(weight) {}
};

class Graph {

public:

	Graph (long long new_size = 0, long long new_a = 0, long long new_b = 0) : size(new_size), a(new_a), b(new_b), links(new_size) {}

	long long GetSize () const { return size; }
	long long GetFirstPrice  (long long k = 0) const { return k + a; }
	long long GetSecondPrice (long long k = 0) const { return k + b; }
	vector<Link> GetLinks (long long v) const {return links[v]; }

	void MakeLinks ();

private:

	long long size;
	long long a;
	long long b;

	vector<vector<Link>> links;
};

void Graph::MakeLinks () {

	for (long long vert = 0; vert < size; vert++) {
		links[vert].push_back(Link((vert + 1) % size, GetFirstPrice(0)));
		links[vert].push_back(Link((vert * vert + 1) % size, GetSecondPrice(0)));	
	}
}

long long ShortestWay (const Graph& graph, long long start, long long end) {

	long long n = graph.GetSize();

	vector<long long> dist (n, numeric_limits<long long>::max());
	dist[start] = 0;

	// first - distance second - destination vertex
	set<pair<long long, long long>> edges; 						
	edges.insert ({dist[start], start});

	while (!edges.empty()) {

		// with minimal distance
		long long vert = edges.begin() -> second; 	

		// already found answer			
		edges.erase (edges.begin()); 							

		for (auto edge : graph.GetLinks(vert)) {
			if (dist[vert] + edge.weight < dist[edge.to]) {   						
				edges.erase  ({dist[edge.to], edge.to});
				dist[edge.to] = dist[vert] + edge.weight;
				edges.insert ({dist[edge.to], edge.to});
			}
		}
	}

	return dist[end];
}

int main() {
	
	long long a = 0;
	long long b = 0;
	cin >> a >> b;

	long long m = 0;
	cin >> m;

	Graph graph(m, a, b);

	long long start = 0;
	long long end = 0;
	cin >> start >> end;

	graph.MakeLinks();

	cout << ShortestWay(graph, start, end);
}