/*
Рику необходимо попасть на межвселенную конференцию. За каждую телепортацию он платит бутылками лимонада, 
поэтому хочет потратить их на дорогу как можно меньше (он же всё-таки на конференцию едет!). Однако после 
K перелетов подряд Рика начинает сильно тошнить, и он ложится спать на день. Ему известны все существующие 
телепортации. Теперь Рик хочет найти путь (наименьший по стоимости в бутылках лимонада), учитывая, что 
телепортация не занимает времени, а до конференции осталось 10 минут (то есть он может совершить не более K перелетов)!
*/
#include <iostream>
#include <vector>
#include <set>

using namespace std;

struct Edge {

	int weight;
	int to;

	Edge (int n_weight, int n_to) : weight(n_weight), to(n_to) {}
};

class Graph {

public:

	Graph (int new_size = 0) : links(new_size), size(new_size) {}

	void Add (int from, int to, int data);

	int GetSize () const {return size;}
	vector<Edge> GetNeighbours (int v) const {return links[v];}

private:

	vector<vector<Edge>> links;
	int size = 0;

};

void Graph::Add (int from, int to, int data) {

	links[from].push_back(Edge(data, to));
}

int ShortestWay (const Graph& graph, int start, int end, int k) {

	vector<vector<int>> dist (graph.GetSize(), vector<int>(k + 1, numeric_limits<int>::max()));
	dist[start][0] = 0;

	for (int step = 0; step < k; step++) {

		for (int from = 0; from < graph.GetSize(); from++) {
			for (auto edge : graph.GetNeighbours(from)) {

				if (dist[edge.to][step + 1] > dist[from][step] + edge.weight) {
					dist[edge.to][step + 1] = dist[from][step] + edge.weight;
				}

			}
		}
	}

	int result = numeric_limits<int>::max();
	for (int i = 0; i <= k; i++) {
		result = min(result, dist[end][i]);
	}

	return result;
}

int main() {

	int n = 0;
	int m = 0;
	cin >> n >> m;

	Graph graph(n);

	int k = 0;
	cin >> k;

	int start = 0;
	int end = 0;
	cin >> start >> end;

	for (int i = 0; i < m; i++) {

		int from, to, data;
		cin >> from >> to >> data;
		graph.Add (from - 1, to - 1, data);
	}

	int answer = ShortestWay (graph, start - 1, end - 1, k);
	if (answer < numeric_limits<int>::max()) {
		cout << answer;
	} else {
		cout << -1;
	}
}