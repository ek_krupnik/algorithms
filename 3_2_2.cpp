#include <iostream>
#include <stack>
#include <vector>
#include <assert.h>
#include <algorithm>


using namespace std;

struct Node {

	int key;
	int priority;
	Node* left;
	Node* right;

	Node (int new_key = 0, int new_priority = 0) {
		key = new_key;
		priority = new_priority;
		left = nullptr;
		right = nullptr; 
	}

	~Node () {
		delete left;
		delete right;
	}
};

void DFS (Node *root, int deep, vector<int> &level_width);

class CartesianTree {

public:

	CartesianTree () : root(nullptr) {};
	~CartesianTree () {
		delete root;
	};

	void AddNode (int key, int priority);
	Node* GetRoot ();
	vector<int> GetWidth() const;
	void GoDFS ();

private:

	Node* root;
	vector<int> level_width;
	Node* Insert (Node *root, int key, int priority);
	Node* Merge (Node *first_root, Node *second_root);
	pair<Node *, Node *> Split (Node *root, int data);
};

void CartesianTree::AddNode (int key, int priority) {

	root = Insert (root, key, priority);
}

void CartesianTree::GoDFS () {

	DFS (root, 0, level_width);
}

Node *CartesianTree::GetRoot () {
	return root;
}

vector<int> CartesianTree::GetWidth () const {
	return level_width;
}

Node* CartesianTree::Merge (Node *first_root, Node *second_root) {

	if (first_root == nullptr) {
		return second_root;
	}
	if (second_root == nullptr) {
		return first_root;
	}

	if (first_root->priority > second_root->priority) {
		first_root->right = Merge (first_root->right, second_root);
		return first_root;
	}
	else {
		second_root->left = Merge (first_root, second_root->left);
		return second_root;
	}
}

pair<Node *, Node *> CartesianTree::Split (Node *root, int data) {

	if (root == nullptr) {
		return { nullptr, nullptr };
	}

	if (root->key < data) {
		pair <Node *, Node *> splited = Split (root->right, data);
		root->right = splited.first;
		return { root, splited.second };
	}
	else {
		pair <Node *, Node *> splited = Split (root->left, data);
		root->left = splited.second;
		return { splited.first, root };	
	}
}

Node *CartesianTree::Insert (Node *root, int key, int priority) {

	Node *data = new Node (key, priority);

	if (root == nullptr) {
		return data;
	}

	if (priority >= root->priority) {
		pair <Node *, Node *> splited = Split (root, key);
		data->left = splited.first;
		data->right = splited.second;
		return data;
	}
	if (key < root->key) {
		root->left = Insert (root->left, key, priority);
	}
	else {
		root->right = Insert (root->right, key, priority);
	}	

	return root;
}	

class SearchTree {

public:

	SearchTree () : root(nullptr) {};
	~SearchTree () {
		delete root;
	};

	void Insert (int key, int priority);
	Node* GetRoot ();
	vector<int> GetWidth () const;
	void GoDFS ();

private:

	Node* root;
	vector<int> level_width;
	Node* FindNearestNode (Node *data);
};

void SearchTree::GoDFS () {

	DFS (root, 0, level_width); 
}

Node* SearchTree::GetRoot () {
	return root;
}

vector<int> SearchTree::GetWidth () const {
	return level_width;
}

Node* SearchTree::FindNearestNode (Node *data) {

	assert (data != nullptr);
	if (root == nullptr) {
		return nullptr;
	}

	Node *local_root = root;
	while (local_root->left != nullptr || local_root->right != nullptr) {

		if (data->key < local_root->key) {
			if (local_root->left == nullptr) {
				return local_root;
			}
			local_root = local_root->left;
		}
		else {
			if (local_root->right == nullptr) {
				return local_root;
			}
			local_root = local_root->right;
		}
	}

	return local_root;
}

void SearchTree::Insert (int key, int priority) {

	Node *data = new Node (key, priority);

	if (root == nullptr) {
		root = data;
		return;
	}

	Node *local_root = FindNearestNode (data);

	if (key < local_root->key) {
		assert (local_root->left == nullptr);
		local_root->left = data;
		return;
	}

	assert (local_root->right == nullptr);
	local_root->right = data;
	return;
}

void DFS (Node *root, int deep, vector<int> &level_width) {

	if (root == nullptr) {
		return;
	}

	if (deep >= level_width.size()) {
		level_width.push_back(1);
	}
	else {
		level_width[deep] ++;
	}

	DFS (root->left, deep + 1, level_width);
	DFS (root->right, deep + 1, level_width);
} 

int GetMax (vector<int> &array) {

	auto it = max_element(array.begin(), array.end());
	return array[distance (array.begin(), it)];
}

int main () {
	
	int n;
	cin >> n;

	CartesianTree cartesian_tree = CartesianTree ();
	SearchTree search_tree = SearchTree ();

	int it = 1;
	for (int i = 0; i < n; i++) {
		int key, priority;
		cin >> key >> priority;

		cartesian_tree.AddNode (key, priority);
		search_tree.Insert (key, priority);
	}
	
	cartesian_tree.GoDFS();
	search_tree.GoDFS();

	vector<int> width;
	width = cartesian_tree.GetWidth();
	int max_cartesian = GetMax (width);
	
	width = search_tree.GetWidth();
	int max_search = GetMax (width);

	cout << max_cartesian - max_search;
}