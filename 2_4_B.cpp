#include <iostream>
#include <stack>
#include <vector>

using namespace std;

struct Node {

    int sum;
    int add;

    int l;
    int r;

    Node* l_child;
    Node* r_child;

    Node () : sum(0), add(0), l(0), r(0), l_child(nullptr), r_child(nullptr){};

    ~Node () {
        delete l_child;
        delete r_child;
    }
    
};

class SegmentTree {

public:

    SegmentTree (vector<int> &start_state, int start, int end);
    ~SegmentTree () {
        delete root;
    };

    Node* GetRoot () const {return root;}
    void Add (Node *root, int a, int b, int add);
    bool MakeQuery (int a, int b, int data, int capacity);

private:

    int Query (Node *root, int a, int b);
    Node* Build (vector<int> &data, Node *root, int l, int r);

    Node* root;
};

SegmentTree::SegmentTree (vector<int> &start_state, int start, int end) {

    root = Build (start_state, GetRoot(), start, end);
}

void Update (Node *node) {
    if (node->l != node->r) {
        node->sum += node->add;
        if (node->l != node->r){
            node->l_child->add += node->add;
            node->r_child->add += node->add;
        }
        node->add = 0;
        node->sum = max(node->l_child->sum + node->l_child->add, node->r_child->sum + node->r_child->add);
    }
}

void SegmentTree::Add (Node *root, int l, int r, int val) {

    if (root->l > r || root->r < l) {
        return;
    }
    
    if (root->l >= l && root->r <= r) {
        root->add += val;
        return;
    }
    
    Add (root->l_child, l, r, val);
    Add (root->r_child, l, r, val);
    
    Update (root);
}

Node* SegmentTree::Build (vector<int> &data, Node *root, int l, int r) {

    root = new Node ();

    if (l == r) {
        root->sum = data[l];
    } else {
        
        int m = (l + r) / 2;
        root->l_child = Build (data, root->l_child, l, m);
        root->r_child = Build (data, root->r_child, m + 1, r);
        
        root->sum = max(root->l_child->sum, root->r_child->sum);
    }
    
    root->l = l;
    root->r = r;

    return root;
}

int SegmentTree::Query (Node *root, int l, int r) {

    Update (root);
    
    if (root->l > r || root->r < l) {
        return 0;
    }
    
    if (root->l >= l && root->r <= r) {
        return root->sum + root->add;
    }
        
    return max(Query (root->l_child, l, r), Query (root->r_child, l, r));
}

bool SegmentTree::MakeQuery (int a, int b, int data, int capacity) {

    Node* root = GetRoot();
    if (Query (root, a, b) + data > capacity) {
        return false;
    }

    Add (root, a, b, data);
    return true;
}

int main() {
    
    int n = 0;
    cin >> n;

    n --;
    vector<int> start_state (n, 0);

    for (int i = 0; i < n; i++) {
        cin >> start_state[i];
    }

    int capacity = 0;
    cin >> capacity;

    SegmentTree tree(start_state, 0, n - 1);

    int m = 0;
    cin >> m;
    
    for (int i = 0; i < m; i++) {
        int a, b, data;
        cin >> a >> b >> data;
        if (!tree.MakeQuery (a, b - 1, data, capacity)) {
            cout << i << ' ';
        }
    }
}
