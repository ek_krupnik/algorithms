//#include "Huffman.h"
#include <iostream>
#include <math.h>
#include <string.h>
#include <vector>
#include <algorithm>

typedef unsigned char byte;

using namespace std;

struct Node {
	
	string code;
	byte data;
	int amount;

	Node *left;
	Node *right;

	Node (int new_amount = 0, byte new_data = 0) {
		code = "";
		data = new_data;
		amount = new_amount;
		left = nullptr;
		right = nullptr;
	}

	Node (Node *left_data, Node *right_data) {
		left = left_data;
		right = right_data;
	}

	~Node () {
		delete left;
		delete right;
	}
};

class HuffmanTree {

public:

	void Encode (IInputStream& original, IOutputStream& compressed);
	void Decode (IInputStream& compressed, IOutputStream& original);

	HuffmanTree () : root(nullptr) {};
	~HuffmanTree () {
		delete root;
	}

private:

	Node *root;
	void Count (vector<byte> &data, vector<Node *> &result);
	void StreamRead (IInputStream& input, vector<byte> &data);
	void StreamWrite (vector<byte> &result, IOutputStream& output);
	Node* MakeNode (Node *left_data, Node *right_data);
	void Add (Node *root, Node *left_data, Node *right_data);
	void MakeTree (vector<Node *> formation);
	void MakeCode (vector<string> &arr, Node *root, string way);	
	void IntoByte (string answer, vector<byte> &result);
};

bool Cmp (Node *first, Node *second) {
	return (first->amount > second->amount) ? true : false;
}

HuffmanTree tree = HuffmanTree ();

void Encode (IInputStream& original, IOutputStream& compressed){
	tree.Encode (original, compressed);
}

void Decode (IInputStream& compressed, IOutputStream& original){
	tree.Decode (compressed, original);
}

void HuffmanTree::StreamRead (IInputStream& input, vector<byte> &data) {

	byte value;
	while (input.Read (value)) { 
		data.push_back(value); 
	}
}

void HuffmanTree::StreamWrite (vector<byte> &result, IOutputStream& output) {

	for (auto i : result) {
		output.Write (i);
	}
}

void HuffmanTree::IntoByte (string answer, vector<byte> &result) {

	int i = 0;
	bool last = false;

	while (i < answer.size()) {

		byte new_elem = 0;
		if (answer.size () - i < 8) {
			if (last) {
				return;
			}

			int size = answer.size ();
			for (int j = 0; j < size - i + 2; j++) {
				answer += "0";
			}

			last = true;
		}

		for (int k = 7; k >= 0; k--) {
			new_elem += pow (2, k) * (answer[i++] - '0');
		}

		result.push_back(new_elem);
	}
}

int size_ = 0;
void HuffmanTree::Encode (IInputStream& original, IOutputStream& compressed) {
	
	vector<byte> data;
	StreamRead (original, data);

	size_ = data.size ();

	vector<string> arr(256, "");
	for (int i = 0; i < 256; i++) arr[i] = "";

	vector<Node *> formation;

	Count (data, formation);
	MakeCode (arr, root, "");	

	string answer = "";

	for (auto i : data) {
		answer += arr[i];
	}

	vector<byte> result;

	IntoByte (answer, result);
	StreamWrite (result, compressed);
}

void HuffmanTree::Decode (IInputStream& compressed, IOutputStream& original) { 
		
	Node *local_root = root;
	vector<byte> result;
	
	vector<byte> data;
	StreamRead (compressed, data);

	byte bit = 0;

	for (auto i : data) {		
		for (int k = 7; k >= 0; k--) 
		{
			bit = (i >> k) & 1;

			if ((int) bit == 0)
			{
				if (local_root->left != nullptr){
					local_root = local_root->left;
				}
				else {
					if (result.size() >= size_) {
						StreamWrite (result, original);
						return;
					}

					result.push_back(local_root->data);
					local_root = root->left;
				}
			}
			else 
			{
				if (local_root->right != nullptr){
					local_root = local_root->right;
				}
				else {
					if (result.size() >= size_) {
						StreamWrite (result, original);
						return;
					}

					result.push_back(local_root->data);
					local_root = root->right;
				}
			}
		}
	}

	StreamWrite (result, original);
}

void HuffmanTree::Count (vector<byte> &data, vector<Node *> &result) {
	
	vector<int> arr(256, 0);

	for (auto i : data) {
		arr[i] += 1;
	}

	for (int i = 0; i < 256; i++) {
		if (arr[i] != 0) {
			result.push_back (new Node(arr[i], i));
		}
	}

	MakeTree (result);
}

void HuffmanTree::MakeTree (vector<Node *> formation) {
	
	sort (formation.begin(), formation.end(), Cmp);

	while (formation.size() > 1) {

		Node *first_data = formation.back();
		formation.pop_back();

		Node *second_data = formation.back();
		formation.pop_back();
		
		Node *new_data = MakeNode (first_data, second_data);
		formation.push_back(new_data);

		int pos = formation.size() - 2;
		while (pos >= 0 && new_data->amount > formation[pos]->amount) {
			swap (formation[pos], formation[pos + 1]);
			pos--;
		}
	}

	root = formation[0];

	return;
}

void HuffmanTree::MakeCode (vector<string> &arr, Node *local_root, string way) {

	if (local_root->left) {
    	
    	way += '0';
    	MakeCode (arr, local_root->left, way);

    	way.pop_back();
    }

	if (local_root->right) {
    	
    	way += '1';
    	MakeCode (arr, local_root->right, way);

    	way.pop_back();
    }

    arr[local_root->data] = way;
}