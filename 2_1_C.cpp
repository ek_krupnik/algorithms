// Сделать связным ориентированный граф за минимальное количество добавленных ребер

#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>

using namespace std;


struct DSU {

	vector<int> dsu;
	vector<int> size;

	DSU () {};

	DSU (int new_size) {
		for (int i = 0; i < new_size; i++) {
			size.push_back(0);
			dsu.push_back(-1);
		}
	}
	
	void MakeSet (int v) {

		dsu[v] = v;
		size[v] = 1;
	}

	int FindSet (int v) {

		if (dsu[v] == v) {
			return v;
		}

		return dsu[v] = FindSet(dsu[v]);
	}

	void MergeSets (int u, int v) {

		u = FindSet(u);
		v = FindSet(v);

		if (size[v] < size[u]) {
			swap(u, v);
		}

		dsu[u] = v;
	}
};

class Graph {

public:

	Graph (int new_size = 0) : links(new_size), reversed(new_size), size(new_size){}

	void Add (int from, int to);
	int GetSize () const {return size; }
	vector<int> GetLinks (int v, bool is_reversed) const;

private:

	vector<vector<int>> links;
	vector<vector<int>> reversed;
	int size;
};

vector<int> Graph::GetLinks (int v, bool is_reversed) const {
	
	if (is_reversed) {
		return reversed[v];
	}

	return links[v];
};

void Graph::Add (int from, int to) {

	links[from].push_back(to);
	reversed[to].push_back(from);
}

void DFS (int v, int start, const Graph& graph, bool is_reversed, vector<bool> &used, vector<int> &in_top_sort, DSU &dsu) {

	used[v] = true;
	if (!is_reversed) {
		in_top_sort.push_back(v);
	}

	if (is_reversed) {
		dsu.MergeSets (v, start);
	}

	for (auto to : graph.GetLinks(v, is_reversed)) {
		if (!used[to]) {
			DFS (to, start, graph, is_reversed, used, in_top_sort, dsu);
		}
	}
}

vector<int> BuidTopSort (const Graph &graph, DSU &dsu) {

	vector<int> in_top_sort;

	vector<bool> used(graph.GetSize(), false);
	for (int i = 0; i < graph.GetSize(); i++) {
		if (!used[i]) {
			DFS (i, i, graph, false, used, in_top_sort, dsu);
		}
	}

	return in_top_sort;
}

void Condensing (const Graph &graph, vector<int> &in_top_sort, DSU &dsu) {
	
	vector<bool> used(graph.GetSize(), false);

	for (int i = 0; i < graph.GetSize(); i++) {
		dsu.MakeSet (i);
	}

	for (auto i : in_top_sort) {
		
		if (!used[i]) {
			DFS (i, i, graph, true, used, in_top_sort, dsu);
		}
	}
}

int MakeContact (const Graph &graph) {

	DSU dsu(graph.GetSize());

	vector<int> in_top_sort = BuidTopSort (graph, dsu);
	Condensing (graph, in_top_sort, dsu);

	vector<int> in_edge (graph.GetSize(), 0);
	vector<int> out_edge (graph.GetSize(), 0);

	vector<bool> if_comp (graph.GetSize(), false);

	for (int i = 0; i < graph.GetSize(); i++) {
		int v = dsu.FindSet (i);
		if_comp[v] = true;
	}

	int prev = dsu.FindSet (0);
	bool is_one_comp = true;

	for (int i = 0; i < graph.GetSize(); i++) {
		if ( dsu.FindSet (i) != prev ) {
			is_one_comp = false;
			break;
		}
		prev = dsu.FindSet (i);
	}

	if (is_one_comp) {
		return 0;
	}

	for (int i = 0; i < graph.GetSize(); i++) {
		for (auto v : graph.GetLinks(i, false)) {

			int from = dsu.FindSet(i);
			int to = dsu.FindSet(v);

			if (from != to) {
				in_edge[to] ++;
				out_edge[from] ++;
			}

		}
	}
	
	int cnt_in = 0;
	for (int i = 0; i < graph.GetSize(); i++) {
		if (in_edge[i] == 0 && if_comp[i]) {
			cnt_in ++;
		}
	}

	int cnt_out = 0;
	for (int i = 0; i < graph.GetSize(); i++) {
		if (out_edge[i] == 0 && if_comp[i]) {
			cnt_out ++;
		}
	}

	return max (cnt_out, cnt_in);
}

int main() {
	
	int n, m;
	cin >> n >> m;

	Graph graph = Graph(n);

	for (int i = 0; i < m; i++) {
		int from, to;
		cin >> from >> to;
		graph.Add(from - 1, to - 1);
	}

	cout << MakeContact (graph);
}