// Два непересекающихся пути из одной вершины в другую

#include <iostream>
#include <vector>

using namespace std;

const int INF = 3;

struct Edge {

	int to;
	int capacity = 1;
	int flow = 0;

	bool back = false;
	int back_ind;

	Edge (int to, int ind, bool back = false) : to(to), back_ind(ind), back(back) {
		if (back) {
			capacity = 0;
		}
	}
};

class Graph {

public:

	Graph (int new_size = 0) : links(new_size), size(new_size) {}

	void Add (int from, int to);
	int GetSize () const {return size;}

	void Print () const;
	vector<Edge> GetLinks (int v) const {return links[v];}

	void AddFlow (int from, int to, int diff);
	void UseEdge (int from, int ind);

private:

	vector<vector<Edge>> links;
	int size = 0;

};

void Graph::UseEdge (int from, int ind) {
	links[from][ind].flow = 0;
}

void Graph::AddFlow (int from, int ind, int diff) {

	links[from][ind].flow += diff;

	int to = links[from][ind].to;
	int ind_to = links[from][ind].back_ind;

	links[to][ind_to].flow -= diff;
}

void Graph::Add (int from, int to) {

	links[from].push_back(Edge(to, (links[to]).size()));
	links[to].push_back(Edge(from, (links[from]).size() - 1, true));
}

int SearchNewWay (int v, int end, Graph& graph, vector<bool> &used, int min_capacity) {

	if (v == end) {
		return min_capacity;
	}

	used[v] = true;

	int ind = 0;
	for (auto edge : graph.GetLinks(v)) {

		if (!used[edge.to] && edge.flow < edge.capacity) {

			int diff = SearchNewWay (edge.to, end, graph, used, min(min_capacity, edge.capacity - edge.flow));
			if (diff > 0) {

				graph.AddFlow(v, ind, diff);
				//used[v] = false;
				return diff;
			}
		}
		ind ++;
	}
	//used[v] = false;
	return 0;
}

void FindWay (int v, int end, Graph& graph, vector<int> &res) {

	res.push_back(v);
	if (v == end) {
		return;
	}

	vector<Edge> neib = graph.GetLinks(v);

	for (int i = 0; i < neib.size(); i++) {
		Edge edge = neib[i];
		if (edge.flow > 0 && !edge.back) {
			graph.UseEdge (v, i);
			FindWay (edge.to, end, graph, res);
			return;
		}
	}
}

bool FindTwoWays (Graph& graph, int start, int end, vector<int>& first, vector<int>& second) {

	int last_flow = -1;
	int flow = 0;

	while (flow > last_flow) {
		last_flow = flow;
		vector<bool> used(graph.GetSize(), false);
		flow += SearchNewWay (start, end, graph, used, INF);
	}

	if (flow < 2) {
		return false;
	}

	FindWay (start, end, graph, first);
	FindWay (start, end, graph, second);	

	return true;
}

int main() {

	int n, m, s, t;
	cin >> n >> m >> s >> t;

	Graph graph(n);

	for (int i = 0; i < m; i++) {
		int from, to;
		cin >> from >> to;
		graph.Add (from - 1, to - 1);
	}

	vector<int> first;
	vector<int> second;
	bool res = FindTwoWays (graph, s - 1, t - 1, first, second);

	if (res) {
		cout << "YES\n";
		for (auto i : first) {
			cout << i + 1 << ' ';
		}
		cout << endl;
		for (auto i : second) {
			cout << i + 1 << ' ';
		}
	} else {
		cout << "NO\n";
	}
}