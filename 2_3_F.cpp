#include <iostream>
#include <vector>
#include <set>
#include <string>

using namespace std;

const int INF = numeric_limits<int>::max();

struct Edge {

	int to;
	int capacity;
	int flow;

	bool back;
	int back_ind;
	
	Edge (int to, int ind, bool back = false) : to(to), back_ind(ind), back(back) {
		if (back) {
			back = true;
			capacity = 0;
			flow = 1;
		} else {
			capacity = 1;
		}
	}
};

class Graph {

public:

	explicit Graph (int n = 0) : links(n), size(n) {}

	void AddChar (int from, int to, char c);
	void Print() const ;

	int GetSize () const {return size;}
	vector<Edge> GetLinks (int v) const {return links[v];}

	void AddFlow (int from, int to, int diff);
	void UseEdge (int from, int ind);

private:

	vector<vector<Edge>> links;
	int size = 0;
};

void Graph::UseEdge (int from, int ind) {
	links[from][ind].flow = 0;
}

void Graph::AddFlow (int from, int ind, int diff) {

	links[from][ind].flow += diff;

	int to = links[from][ind].to;
	int ind_to = links[from][ind].back_ind;

	links[to][ind_to].flow -= diff;
}

void Graph::AddChar (int from, int to, char c) {

	if (c == '1') {
		links[from].push_back(Edge(to, (links[to]).size()));
		links[to].push_back(Edge(from, (links[from]).size() - 1, true));
	}
}

int SearchNewWay (int v, int end, Graph& graph, vector<bool> &used, int min_capacity) {

	if (v == end) {
		return min_capacity;
	}

	used[v] = true;

	int ind = 0;
	for (auto edge : graph.GetLinks(v)) {

		if (!used[edge.to] && edge.flow < edge.capacity) {

			int diff = SearchNewWay (edge.to, end, graph, used, min(min_capacity, edge.capacity - edge.flow));
			if (diff > 0) {

				graph.AddFlow(v, ind, diff);
				return diff;
			}
		}
		ind ++;
	}
	return 0;
}

void FindPart (const Graph& graph, int v, vector<bool> &in_ans, vector<int> &res) {

	res.push_back (v);
	in_ans[v] = true;

	for (auto edge : graph.GetLinks(v)) {
		if (edge.flow < edge.capacity && !edge.back && !in_ans[edge.to]) {
			FindPart (graph, edge.to, in_ans, res);
		}
	}
}

void Graph::Print () const {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < links[i].size(); j++) {
			cout << "from " << i << " to " << links[i][j].to << ' ' << links[i][j].flow << ' ' << links[i][j].capacity << ' ' << links[i][j].back << endl;
		}
		cout << endl;
	} cout << endl << endl;
}

pair<vector<int>, vector<int>> Divide (const Graph& graph) {

	vector<int> res_one;
	vector<int> res_two;

	int min_div = INF;
	Graph banch_graph;
	int banch_s = -1;
	int start = 0;

	for (int end = 0; end < graph.GetSize(); end ++) {

		Graph new_graph = graph;
		if (start != end) {

			int last_flow = -1;
			int flow = 0;

			while (flow > last_flow) {
				last_flow = flow;
				vector<bool> used(new_graph.GetSize(), false);
				flow += SearchNewWay (start, end, new_graph, used, INF);
			}

			if (flow < min_div) {

				banch_graph = new_graph;
				banch_s = start;
				min_div = flow;
			}
		}
	}

	vector<bool> in_ans (graph.GetSize(), false);
	FindPart (banch_graph, banch_s, in_ans, res_one);

	for (int i = 0; i < graph.GetSize(); i++) {
		if (!in_ans[i]) {
			res_two.push_back(i);
		}
	}

	return {res_two, res_one};
}

int main() {

	int n;
	cin >> n;

	Graph graph(n);

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			char c;
			cin >> c;
			graph.AddChar(i, j, c);
		}
	}

	pair<vector<int>, vector<int>> res = Divide (graph);

	for (auto i : res.first) {
		cout << i + 1 << ' ';
	}

	cout << endl;

	for (auto i : res.second) {
		cout << i + 1 << ' ';
	}
}