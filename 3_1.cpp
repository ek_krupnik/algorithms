#include <iostream>
#include <vector>
#include <stdio.h>

using namespace std;

int Median (const int a, const int b, const int c, int l, int r) {

	if (a > b) {
		if (c > a) {
			return l;
		}
		return (b > c) ? (l + r) / 2 : r;
	}

	if (c > b){
		return (l + r) / 2;
	}

	return (a > c) ? l : r;
}

int Partition (vector<int> &array, const int left, const int right) {

	int mid = Median(array[left], array[(left + right) / 2], array[right], left, right);
	swap(array[mid], array[right]);

	int it = left;

	for (int i = left; i <= right; i++) {
		if (array[i] <= array[right]) {
			swap(array[it ++], array[i]);
		}
	}

	return it - 1;
}

int Nth (vector<int> &array, int k) {

	int left = 0;
	int right = array.size() - 1;

	while (left <= right) {

		if (left == right) {
			return array[left];
		}
		assert(left <= right);

		int pos = Partition(array, left, right);

		if (k == pos) {
			return array[k];
		}
		else if (k < pos) {
			if (left >= pos - 1){
				return array[left];
			}

			right = pos - 1;
		}
		else if (k > pos) {
			if (right <= pos + 1) {
				return array[right];
			}

			left = pos + 1;
		}
	}
}


int main(){

	int n;
	int k;
	cin >> n >> k;

	vector<int> array;
	for (int i = 0; i < n; i++) {
		int data;
		cin >> data;
		array.push_back(data);
	}

	cout << Nth(array, k);
}