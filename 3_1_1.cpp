#include <iostream>
#include <stack>
#include <vector>
#include <cassert>

using namespace std;

struct Node {

	int key;
	Node* l_child = nullptr;
	Node* r_child = nullptr;

	Node() = default;
	Node(int new_key = 0) : key(new_key) {}

	~Node() {
		delete l_child;
		delete r_child;
	}
};

class SearchTree {

public:

	SearchTree() = default;
	~SearchTree() {
		delete root;
	};

	void Insert(Node* data);
	void Pass(vector<int> &elem);

private:

	Node* root;
	Node* FindNearestNode(Node *data);
	void Print(Node *root);
};

Node* SearchTree::FindNearestNode(Node *data) {

	assert (data != nullptr);
	if (root == nullptr) {
		return nullptr;
	}

	Node *local_root = root;
	while (local_root->l_child != nullptr || local_root->r_child != nullptr) {

		if (data->key < local_root->key) {
			if (local_root->l_child == nullptr) {
				return local_root;
			}
			local_root = local_root->l_child;
		}
		else {
			if (local_root->r_child == nullptr) {
				return local_root;
			}
			local_root = local_root->r_child;
		}
	}

	return local_root;
}

void SearchTree::Insert(Node* data) {

	if (root == nullptr) {
		root = data;
		return;
	}

	Node *local_root = FindNearestNode(data);

	if (data->key < local_root->key) {
		assert(local_root->l_child == nullptr);
		local_root->l_child = data;
		return;
	}

	assert(local_root->r_child == nullptr);
	local_root->r_child = data;
	return;
}

void SearchTree::Pass(vector<int> &elem) {

	Node *local_root = root;

	stack<Node *> go_pass;
	go_pass.push(local_root);

	while (!go_pass.empty() && local_root != nullptr) {

		while (local_root->l_child != nullptr) {
			local_root = local_root->l_child;
			go_pass.push(local_root);
		}

		while (local_root->r_child == nullptr && !go_pass.empty()) {
			elem.push_back(local_root->key);
			go_pass.pop();
			if (!go_pass.empty()) {
				local_root = go_pass.top();
			}
		}

		if (go_pass.empty()) {
			return;
		}

		elem.push_back(local_root->key);
		go_pass.pop();

		local_root = local_root->r_child;
		go_pass.push(local_root);
	}
}

void SearchTree::Print(Node *root) {

	if (root == nullptr) {
		return;
	}
	
	Print (root->l_child);
	Print (root->r_child);

	if (root->l_child != nullptr) {
		cout << root->key << ' ' << root->l_child->key << endl;
	}
	if (root->r_child != nullptr) {
		cout << root->key << ' ' << root->r_child->key << endl;
	}

	if (root->r_child == nullptr && root->l_child == nullptr) {
		cout << root->key << endl;
	}	
}

void VectorPrint(vector<int> &elem) {

	for (int i = 0; i < elem.size(); i++) {
		cout << elem[i] << ' ';
	}
	cout << endl;
}

int main() {
	
	int n;
	cin >> n;

	SearchTree tree = SearchTree();

	for (int i = 0; i < n; i++) {

		int data;
		cin >> data;
		Node* node = new Node(data);

		tree.Insert(node);
	}
	vector<int> elem;
	
	tree.Pass(elem);
	VectorPrint(elem);
}