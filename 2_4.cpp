#include <iostream>
#include <vector>
#include <stdio.h>
#include <functional>

using namespace std;

void Rebuild(vector<pair<int, int> > &heap, int size){

    int num = 0;

    while (2 * num + 1 < size){
        int left_ch = num * 2 + 1;
        int right_ch = num * 2 + 2;
        int min_ch = left_ch;

        if (right_ch < size && heap[right_ch].first > heap[left_ch].first){
            min_ch = right_ch;
        }

        if (heap[num].first >= heap[min_ch].first){
            break;
        }
        
        swap(heap[num], heap[min_ch]);
        num = min_ch;
    }
}

void HeapSort(vector<pair<int, int> > &heap){

    make_heap(heap.begin(), heap.end());

    for(int i = heap.size() - 1; i > 0; i--){
        swap(heap[0], heap[i]);
        Rebuild(heap, i);
    }
}

int FindOnceCovered(vector<pair<int, int> > &heap){

    int ans = 0;
    int cnt = 1;

    for (int i = 1; i < heap.size(); i++){

        if (cnt == 1){
            ans += heap[i].first - heap[i - 1].first;
        }

        cnt += heap[i].second;
    } 

    return ans;  
}

int main(){
    
    int n;
    cin >> n;
    vector<pair<int, int> > heap;
   
    int l, r;

    for(int i = 0; i < n; i++){
        cin >> l >> r;
        heap.push_back(make_pair(l, -1));
        heap.push_back(make_pair(r, 1));   
    }

    HeapSort(heap);

    cout << FindOnceCovered(heap);
}