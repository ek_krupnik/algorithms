#include <iostream>
#include <vector>

using namespace std;

long long CountMethods (long long n) {

	vector<vector<long long>> dp(n + 1, vector<long long>(n + 1, 0));
	for (long long i = 0; i <= n; i ++) {
		dp[i][0] = 0;
	}
	for (long long i = 0; i <= n; i ++) {
		dp[0][i] = 1;
	}

	for (long long i = 1; i <= n; i ++) {
		for (long long j = 1; j <= n; j++) {

			if (j > i) {
				dp[i][j] = dp[i][i];
				continue;
			}
			dp[i][j] = dp[i - j][j - 1] + dp[i][j - 1];
		}
	}

	return dp[n][n];
}

int main() {
	
	long long n;
	cin >> n;

	cout << CountMethods (n);
}