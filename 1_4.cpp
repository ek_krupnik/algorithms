#include <iostream>
#include <string>
#include <vector>

using namespace std;

void InsertionSort(vector<string> &array){

	for(int i = 1; i < array.size(); i++){
		string key = array[i];
		int it = i - 1;
		while(it >= 0 && key < array[it]){
			array[it + 1] = array[it];
			it --;
		}
		array[it + 1] = key;
	}

}

int main(){
	
	int n;
	cin >> n;

	vector<string> array;

	for(int i = 0; i < n; i++){
		string str;
		cin >> str;

		array.push_back(str); 
	}

	InsertionSort(array);

	for(auto i : array){
		cout << i << endl;
	}
}