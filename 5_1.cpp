#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int GetNum(string s, int ind){
	if (ind >= s.size()){
		return 0;
	}
	return int(s[ind] % 256) + 1;
}

vector<string> CountingSort(vector<string> &array, int ind){

	vector<string> sorted(array.size(), "");

	vector<vector<string> > cnt(257, vector<string>());
	for(int i = 0; i < array.size(); i++){
		cnt[GetNum(array[i], ind)].push_back(array[i]);
	}

	int pos = 0;
	for(int i = 0; i < 257; i++){
		for(int j = 0; j < cnt[i].size(); j++){
			sorted[pos] = cnt[i][j];
			pos ++;
		}
	}

	return sorted;
}

void Print(vector<string> &array){
	for(int i = 0; i < array.size(); i++){
		cout << array[i] << endl;
	}
}

void MSD(vector<string> &array, int l, int r, int ind, int max_len){

	if (ind == max_len || l == r){
		return;
	}

	vector<string> part(r - l + 1, "");
	for(int i = l; i <= r; i++){
		part[i - l] = array[i];
	}

	part = CountingSort(part, ind);
	int local_l = 0;
	char value = '\0';

	if (ind < part[0].size()){
		value = part[0][ind];
	}

	int i = 0;
	while(i < part.size()){
		while (i < part.size() && (ind >= part[i].size() || part[i][ind] == value)){
			i ++;
		}
		MSD(part, local_l, i - 1, ind + 1, max_len);
		if (i == part.size()){
			break;
		}
		local_l = i; 
		value = part[local_l][ind];
	}

	for(int i = l; i <= r; i++){
		array[i] = part[i - l];
	}
}

int main(){

	string a = "";
	vector<string> array;

	unsigned long max_len = 0;

	while(cin >> a){
		array.push_back(a);
		max_len = max(max_len, a.size());
	}

	MSD(array, 0, array.size() - 1, 0, max_len);
	Print(array);
}