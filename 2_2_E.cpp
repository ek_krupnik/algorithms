/*
Рик обладает некоторой суммой в валюте S. Он задаётся вопросом, можно ли, после нескольких 
операций обмена увеличить свой капитал. Конечно, он хочет, чтобы в конце его деньги вновь были в валюте S. 
Помогите ему ответить на его вопрос. Рик должен всегда должен иметь неотрицательную сумму денег.
*/

#include <iostream>
#include <vector>

using namespace std;

struct Edge {

	long double rate;
	long double comm;
	int to;

	Edge (long double n_rate = 0, long double n_comm = 0, int n_to = 0) : 
				rate(n_rate), comm(n_comm), to(n_to) {}
};

class Graph {

public:

	Graph (int new_size = 0) : links(new_size), size(new_size) {}

	void Add (int from, int to, long double rate, long double comm);

	int GetSize () const {return size;}
	vector<Edge> GetNeighbours (int v) const {return links[v];}

private:

	vector<vector<Edge>> links;
	int size = 0;

};

void Graph::Add (int from, int to, long double rate, long double comm) {

	links[from].push_back(Edge(rate, comm, to));
}

bool IsNegativeCycle (const Graph& graph, int start, long double sum) {

	vector<long double> summ (graph.GetSize(), -1);
	summ[start] = sum;

	bool result = false;

	for (int i = 0; i <= graph.GetSize(); i++) {

		if (sum - summ[start] > 0) {
			return true;
		}
		result = false;

		for (int from = 0; from < graph.GetSize(); from++) {

			for (auto edge : graph.GetNeighbours(from)) {

				if (summ[from] != -1) {
					if (summ[edge.to] < (summ[from] - edge.comm) * edge.rate) {

						if ((summ[from] - edge.comm) * edge.rate < 0) {
							continue;
						}

						summ[edge.to] = (summ[from] - edge.comm) * edge.rate;
						result = true;
					}
				}
			}
		}
	}

	return result;
}

int main () {
	
	int n, m, s;
	cin >> n >> m >> s;
	Graph graph(n);

	long double v;
	cin >> v;

	for (int i = 0; i < m; i++) {
		int a, b;

		cin >> a >> b;

		long double rab, cab;
		cin >> rab >> cab;


		long double rba, cba;
		cin >> rba >> cba;

		graph.Add (a - 1, b - 1, rab, cab);
		graph.Add (b - 1, a - 1, rba, cba);
	}

	if (IsNegativeCycle(graph, s - 1, v)) {
		cout << "YES\n";
	} else {
		cout << "NO\n";
	}
}