#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

const int INF = numeric_limits<int>::max();

struct Node {

	string data;
	int size;

	int key;
	int priority;
	Node* left;
	Node* right;

	Node (int key = 0, string data = "") : key(key), data(data), priority(rand()), 
											left(nullptr), right(nullptr), size(1) {}

	~Node () {
		delete left;
		delete right;
	}
};

class CartesianTree {

public:

	CartesianTree () : root(nullptr) {};
	~CartesianTree () {};

	void InsertAt (int ind, const string &data);
	void EraseAt (int start, int end);

	string GetAt (int key);

private:

	Node* Merge (Node *first_root, Node *second_root);
	pair<Node *, Node *> Split (Node *root, int data);

	Node* Insert (Node *root, Node *data, int ind);
	Node* Erase (Node *root, int key);

	Node* Find (Node *root, int key);

	Node* root;
};

int GetSize (const Node* root) {
	if (root) {
		return root->size;
	}
	return 0;
}

void Update (Node* root) {
	if (root == nullptr) {
		return;
	}

	root->size = GetSize(root->left) + GetSize(root->right) + 1;
	return;
}

Node* CartesianTree::Merge (Node *first_root, Node *second_root) {

	if (first_root == nullptr) {
		return second_root;
	}
	if (second_root == nullptr) {
		return first_root;
	}

	if (first_root->priority >= second_root->priority) {
		first_root->right = Merge (first_root->right, second_root);
		Update (first_root);
		return first_root;
	}
	else {
		second_root->left = Merge (first_root, second_root->left);
    	Update (second_root);
		return second_root;
	}
}

pair<Node *, Node *> CartesianTree::Split (Node *root, int data) {

	if (root == nullptr) {
		return { nullptr, nullptr };
	}

	if (GetSize (root->left) < data) {
		pair <Node *, Node *> splited = Split (root->right, data - GetSize (root->left) - 1);
		root->right = splited.first;
		Update (root);
		return { root, splited.second };
	}
	else {
		pair <Node *, Node *> splited = Split (root->left, data);
		root->left = splited.second;
		Update (root);
		return { splited.first, root };
	}
}

Node *CartesianTree::Insert (Node *root, Node *data, int ind) {

	if (root == nullptr) {
		return data;
	}
	if (data == nullptr) {
		return root;
	}

	if (data->priority >= root->priority) {
		pair <Node *, Node *> splited = Split (root, ind);
		data->left  = splited.first;
		data->right = splited.second;

		Update (data);
		return data;
	}

	if (ind <= GetSize (root->left)) {
		root->left  = Insert (root->left, data, ind);
	}
	else {
		root->right = Insert (root->right, data, ind - GetSize (root->left) - 1);
	}
	
	Update (root);
	return root;
}

Node *CartesianTree::Erase(Node *root, int key) {

	if (root == nullptr) {
		return nullptr;
	}

	if (GetSize (root->left) == key) {
		return Merge (root->left, root->right);
	}

	if (key < GetSize (root->left)) {
		root->left  = Erase (root->left, key);
	} else {
		root->right = Erase (root->right, key - GetSize (root->left) - 1);
	}

	Update (root);
	return root;
}

void CartesianTree::InsertAt (int ind, const string &data) {

	Update (root);
	root = Insert (root, new Node (ind, data), ind);
}

void CartesianTree::EraseAt (int start, int end) {

	for (int i = end; i >= start; i--) {
		Update (root);
		root = Erase (root, i);
	}
}

Node *CartesianTree::Find (Node *root, int key) {

	if (root == nullptr) {
		return nullptr;
	}
	Update (root);
	if (GetSize (root->left) == key) {
		return root;
	}

	if (key < GetSize (root->left)) {
		return Find (root->left, key);
	} else {
		return Find (root->right, key - GetSize (root->left) - 1);
	}

	Update (root);
}

string CartesianTree::GetAt (int ind) {

    Update (root);
    return (Find (root, ind))->data;
}

int main () {

	CartesianTree tree;

	int n = 0;
	cin >> n;

	for (int i = 0; i < n; i++) {
		char cmd;
		cin >> cmd;

		if (cmd == '+') {
			int ind = 0;
			cin >> ind;

			string str;
			cin >> str;

			tree.InsertAt (ind, str);

		} else if (cmd == '?') {
			int ind = 0;
			cin >> ind;
			cout << tree.GetAt (ind) << endl;

		} else {
			int start = 0;
			int end = 0;

			cin >> start >> end;
			tree.EraseAt (start, end);
		}
	}
}



