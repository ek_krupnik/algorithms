#include <iostream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

void GiveAnswer (bool ans) {

	if (ans) {
		cout << "OK" << "\n";
		return;
	}

	cout << "FAIL" << '\n';
	return;
}

class HashTable {

public:

	HashTable () {

		table = vector<string> (8, "");
		size = 8;
		load_factor = 0;
		max_deep = 0;
	}

	bool Add (const string &element);
	bool Delete (const string &element);
	bool Query (const string &element);
	void ExpandTable ();

private:

	int GetHash (const string &element, int deep_level);
	int FirstHash (const string &element);
	int SecondHash (const string &element);

	vector<string> table;
	int size;
	double load_factor;
	int max_deep;
};

void HashTable::ExpandTable () {

	vector<string> new_table;
	vector<string> old_table;

	for (auto i : table) {
		old_table.push_back(i);
	}

	for (int i = 0; i < 4 * table.size(); i++) {
		new_table.push_back("");
	}

	table = new_table;
	size *= 4;

	load_factor = 0;

	for (auto i : old_table) {
		if (i != "") {
			Add (i);
		}
	}

	return;
}

bool HashTable::Add (const string &element) {

	int index = GetHash (element, 0);
	int i = 0;

	while (table[index] != "") {

		if (table[index] == element) {
			return false;
		}

		index = GetHash (element, ++ i);
		max_deep = max(max_deep, i);
	}

	table[index] = element;

	load_factor += 1.0 / size;
	if (load_factor >= 0.75) {
		ExpandTable ();
	}

	return true;
}

bool HashTable::Delete (const string &element) {

	int i = 0;

	int index = GetHash (element, i);
	int first_index = index;

    while (table[index] != element && i <= max_deep) {

        index = GetHash (element, ++i);    
    }

	if (table[index] == element) {
		table[index] = "";
		return true;
	}

	return false;
}

bool HashTable::Query (const string &element) {

	int index = GetHash (element, 0);
	int first_index = index;
	int i = 0;

	while (table[index] != element && i <= max_deep) {

		index = GetHash (element, ++i);
	}

	if (table[index] == element) {
		return true;
	}

	return false;
}

int HashTable::FirstHash (const string &element) {

	int hash = 0;
	int constant = 31;
	int grade = 0;

	for (auto symbol : element) {
		hash += ((int) floor (((symbol - 'a' + 1) % size)  * pow ((constant % size), ++ grade))) % size;
 	}

	return hash;
}

int HashTable::SecondHash (const string &element) {

	int hash = 0;
	int constant = 53;
	int grade = 0;

	for (auto symbol : element) {
		hash += ((int) floor (((symbol - 'a' + 1) % size) * pow ((constant % size), ++ grade))) % size;
 	}

	return hash * 2 + 1;
}

int HashTable::GetHash (const string &element, int deep_level) { //if not a collision deep_level == 0, just FirstHash

	return (FirstHash (element) + deep_level * SecondHash (element)) % size; // SecondHash returns numbers % 2 == 1;
}

int main() {

	HashTable hash_table = HashTable ();

	char operand = '\0';
	vector <pair<char, string>> query;

	while (cin >> operand) {

		string element = "";
		cin >> element;

		query.push_back ({operand, element});
	}

	for (int i = 0; i < query.size(); i++){

		if (query[i].first == '+') {
			GiveAnswer (hash_table.Add (query[i].second));
		}
		else if (query[i].first == '-') {
			GiveAnswer (hash_table.Delete (query[i].second));
		}
		else {
			GiveAnswer (hash_table.Query (query[i].second));
		}
	}

}