#include <iostream>
#include <vector>
#include <assert.h>

using namespace std;

struct Node {

	int key;
	int height;
	int size;
	Node* left;
	Node* right;

	Node (int new_key = 0, int new_height = 1, int new_size = 1) {
		key = new_key;
		height = new_height;
		size = new_size;
		left = nullptr;
		right = nullptr; 
	}

	~Node () {
		delete left;
		delete right;
	}
};

class AVL {

public:

	void Add (int key);
	void Delete (int key); 
	int FindAnswer (int k);

	AVL () : root(nullptr) {};
	~AVL () {
		delete root;
	};

private:

	Node* root;
	Node* Balance (Node *root);
	void Update (Node *root);

	int GetHeight (Node *root) const;
	int GetSize (Node *root) const;
	int HeightDifference (Node *root) const;
	int Kth (Node *root, int k);

	Node* Insert (Node *root, int key);
	Node* Erase (Node *root, int key);

	Node* RightRotate (Node *root);
	Node* LeftRotate (Node *root);

	Node* FindMin (Node *root) const;
	Node* RemoveMin (Node *root);
};

int AVL::FindAnswer (int k) {

	return Kth (root, k);
}

void AVL::Add (int key) {

	root = Insert (root, key);
}

void AVL::Delete (int key) {

	root = Erase (root, key);
}

int AVL::GetHeight (Node *root) const {

	return (root == nullptr) ? 0 : root->height;
}

int AVL::GetSize (Node *root) const {

	return (root == nullptr) ? 0 : root->size;
}

int AVL::HeightDifference (Node *root) const {

	return GetHeight (root->left) - GetHeight (root->right); 
}

void AVL::Update (Node *root) {

	if (root == nullptr) {
		return;
	}

	root->height = max (GetHeight (root->left), GetHeight (root->right));
	root->size = GetSize (root->left) + GetSize (root->right) + 1;
}

Node* AVL::RightRotate (Node *root) {

	if (root == nullptr) {
		return root;
	}

	Node *new_root = root->left;
	root->left = new_root->right;
	new_root->right = root;

	Update (root);
	Update (new_root);
	return new_root;
}

Node* AVL::LeftRotate (Node *root) {

	if (root == nullptr) {
		return root;
	}

	Node *new_root = root->right;
	root->right = new_root->left;
	new_root->left = root;

	Update (root);
	Update (new_root);
	return new_root;
}

Node* AVL::Balance (Node *root) {

	if (root == nullptr) {
		return root;
	}

	Update (root);

	if (HeightDifference (root) == -2) {

		if (HeightDifference (root->right) < 0) {
			root->right = RightRotate (root->right);
		}
		return LeftRotate (root);
	}
	
	if (HeightDifference (root) == 2) {

		if (HeightDifference (root->left) > 0) {
			root->left = LeftRotate (root->left);
		}
		return RightRotate (root);
	}

	return root;
}

Node* AVL::Insert (Node *root, int key) {

	if (root == nullptr) {
		return new Node (key);
	}

	if (key < root->key) {
		root->left = Insert (root->left, key);
	}
	else {
		root->right = Insert (root->right, key);
	}

	return Balance (root);
} 

Node* AVL::FindMin(Node *root) const {

	if (root == nullptr) {
		return root;
	}

	return root->left ? FindMin (root->left) : root;
}

Node* AVL::RemoveMin(Node *root) {

	if (root == nullptr) {
		return root;
	}

	if(root->left == nullptr){
		return root->right;
	}

	root->left = RemoveMin (root->left);
	return Balance (root);
}

Node* AVL::Erase (Node *root, int key) {

	if (root == nullptr) {
		return root;
	}

	if (key < root->key) {
		root->left = Erase (root->left, key);
	}

	if (key > root->key) {
		root->right = Erase (root->right, key);
	}

	if (key == root->key) {

		Node *left_root = root->left;
		Node *right_root = root->right;
		
		if (right_root == nullptr) {
			return left_root;
		}

		Node *min_node = FindMin (right_root);
		min_node->right = RemoveMin (right_root);
		min_node->left = left_root;

		return Balance (min_node); 
	}

	return Balance (root);
}

int AVL::Kth (Node *root, int k) {

	assert (root != nullptr);
	assert (GetSize (root) >= k);

	if (GetSize (root->left) == k) {
		return root->key;
	}

	if (GetSize(root->left) > k) {
		return Kth (root->left, k);
	}

	return Kth (root->right, k - GetSize (root->left) - 1);
}

int main() {
	
	int n;
	cin >> n;

	AVL tree = AVL ();

	for (int i = 0; i < n; i++) {
		int a, k;
		cin >> a >> k;

		if (a >= 0) {
			tree.Add (a);
		}
		else {
			tree.Delete (-a);
		}

		cout << tree.FindAnswer (k) << ' ';
	}
}