#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>
#include <string>
#include <stack>
#include <cmath>

using namespace std;

const int Max_n = pow(10, 6);

int Median(const int a, const int b, const int c, int l, int r){

	if (a > b){
		if (c > a){
			return l;
		}
		return (b > c) ? (l + r) / 2 : r;
	}

	if (c > b){
		return (l + r) / 2;
	}

	return (a > c) ? l : r;
}

int Partition(vector<int> &array, const int left, const int right, const string &type_mid, const string &type_part){

	int mid = 0;

	if (type_mid == "Middle") {
		mid = (left + right) / 2;
	}
	if (type_mid == "Median_3") {
		mid = Median(array[left], array[(left + right) / 2], array[right], left, right);
	}	

	if (type_part == "Lomuto") {

		swap(array[mid], array[right]);

		int it = left;

		for(int i = left; i <= right; i++){
			if (array[i] <= array[right]){
				swap(array[it ++], array[i]);
			}
		}

		return it - 1;
	}

	if (type_part == "Hoare") {

		int i = left - 1;
		int j = right + 1;

		while (true) {

			while (array[++i] < array[mid]);
			while (array[--j] > array[mid]);

			if (i >= j){
				return j;
			}

			swap(array[i], array[j]);
		}
	}

	assert (false);
}

void BubbleSort(vector<int> &array, int l, int r){

	for(int i = l; i <= r; i++){
		for(int j = i + 1; j <= r; j++){
			if (array[i] > array[j]){
				swap(array[i], array[j]);
			}
		}
	}

}
void Qsort(vector<int> &array, int left, int right, const string &type_mid, const string &type_part, const string &type_sort){

	if (left >= right){
		return;
	}

	if (right - left <= 10 && type_sort == "Tail"){
		BubbleSort(array, left, right);
		return;
	}

	else if (left < right){
		int pos = Partition(array, left, right, type_mid, type_part);
		Qsort(array, left, pos, type_mid, type_part, type_sort);
		Qsort(array, pos + 1, right, type_mid, type_part, type_sort);
	}
}

void Half_recursion(vector<int> &array, int left, int right, const string &type_mid, const string &type_part, const string &type_half, const string &type_sort){

	stack<pair<int, int> > st;
	st.push(make_pair(left, right));

	while (!st.empty()){

		pair<int, int> p = st.top();
		int l = p.first;
		int r = p.second;

		st.pop();

		if (r <= l){
			continue;
		}

		int ind = Partition(array, l, r, type_mid, type_part);
		if (ind - l > r - ind){

        	st.push(make_pair(l, ind - 1));

        	if (type_half == "Half") {
        		Qsort(array, ind + 1, r, type_mid, type_part, type_sort);
        	}
        	if (type_half == "All") {
        		st.push(make_pair(ind + 1, r));
           	}
        }
        else{

        	st.push(make_pair(ind + 1, r));

        	if (type_half == "Half") {
        		Qsort(array, l, ind - 1, type_mid, type_part, type_sort);
        	}
        	if (type_half == "All") {
        		st.push(make_pair(l, ind - 1));
        		break;
        	}
        }
	}
}

void Print(vector<int> &array){
	for (int i = 0; i < array.size(); i++){
		cout << array[i] << endl;
	}
}

int main(int argc, char *argv[]){

	time_t start = clock();

	vector<int> array;
	for (int i = 0; i < Max_n; i++){
		array.push_back(rand());
	}

	if (argc < 4 || argv[1] == "Help") {
		cout << "\n Please, choose options for sort :\n \n";
		cout << " --- Type of recursion : Qsort, Half_recursion \n";
		cout << " --- Type of middle element : Middle, Median_3 \n";
		cout << " --- Type of partition : Lomuto, Hoare \n";
		cout << " --- Type of quick sort (for Qsort) : Tail, All  \n";
		cout << " --- Type of half recursion (for Half recursion) : Half, All \n\n";
		return 0;
	}

	if (string (argv[1]) == "Qsort") {
		Qsort(array, 0, array.size() - 1, string(argv[2]), string(argv[3]), string(argv[4]));	
	}

	if (string (argv[1]) == "Half_recursion") {
		Half_recursion(array, 0, array.size() - 1, string(argv[2]), string(argv[3]), string(argv[5]), string(argv[4]));	
	}

	time_t end = clock();

	cout << "Running time : " << end - start << endl;
	//Print(array);
}