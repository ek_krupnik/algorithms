#include <iostream>
#include <vector>
#include <set>

using namespace std;

struct Node {

	long long weight;
	int to;

	Node (long long weight, int to) : weight(weight), to(to) {}
};

class Graph {

public:

	Graph (int new_size = 0) : links(new_size), size(new_size) {}

	void Add (int from, int to, long long weight);
	long long GetSize () const {return size;}
	vector<Node> GetLinks (int v) const {return links[v];}

private:

	vector<vector<Node>> links;
	long long size = 0;

};

void Graph::Add (int from, int to, long long weight) {

	links[from].push_back(Node(weight, to));
}

long long FindMinOstWeight (const Graph& graph) {

	long long answer = 0;

	// first - weight second - destination vertex
	set<pair<long long, int>> edges; 				
	vector<bool> in_min_ost (graph.GetSize(), false);
	int cnt_in_min_ost = 0;

	in_min_ost[0] = true;
	cnt_in_min_ost ++;

	for (auto edge : graph.GetLinks(0)) {
		edges.insert({edge.weight, edge.to});
	}

	while (cnt_in_min_ost < graph.GetSize()) {

		int vert = edges.begin() -> second;

		if (in_min_ost[vert]) {
			edges.erase (edges.begin()); 
			continue;
		}

		answer += edges.begin() -> first;

		in_min_ost[vert] = true;
		cnt_in_min_ost ++;

		edges.erase (edges.begin()); 						

		for (auto edge : graph.GetLinks(vert)) {
			if (!in_min_ost[edge.to]) {
				edges.insert({edge.weight, edge.to});
			}
		}
	}

	return answer;
}

int main() {

	int n = 0;
	int m = 0;
	cin >> n >> m;

	Graph graph(n);

	for (int i = 0; i < m; i++) {

		int from, to;
		int weight;
		cin >> from >> to >> weight;

		graph.Add (from - 1, to - 1, weight);
		graph.Add (to - 1, from - 1, weight);	
	}

	cout << FindMinOstWeight (graph);
}