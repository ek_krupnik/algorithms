#include <vector>
#include <cmath>
#include <cstdarg>
#include <iostream>
#include <string>
#include <cstdio>

#define ACC_CY 0.0001
#define pi M_PI

using namespace std;

//----------------------------------------------------------------------//
//----------------------------------------------------------------------//

double det (double a, double b, double c,
			double d, double e, double f, 
			double g, double h, double i) {

	double result = 0.0;
	result += a * e * i + d * h * c + b * f * g;
	result -= g * e * c + h * f * a + d * b * i;

	return result;
}

pair <double, double> SolveEquation (double a, double b, double c) {

	if (abs(a) < ACC_CY) {

		if (abs(b) < ACC_CY) {
			cout << "ERROR : WRONG EQUATION\n";
		}

		return { (-c)/(b), (-c)/(b) };
	}

	double discr = b * b - 4 * a * c;

	if (discr < 0) {
		cout << "ERROR : DISCR < 0\n";
		return {};
	}

	double first  = (-b + pow (discr, 0.5)) / (2 * a);
	double second = (-b - pow (discr, 0.5)) / (2 * a);

	return { first, second };
}

void Print (double a, string text) {

	cerr << endl;
	cerr << text << " = " << a << endl << endl; 
}

//----------------------------------------------------------------------//
//----------------------------------------------------------------------//

struct Point {

	double x = 0.0;
	double y = 0.0;

	~Point () {};

	Point (double new_x = 0.0, double new_y = 0.0) : x(new_x), y(new_y) {}

	bool operator== (const Point& other) const {
		return (abs(x - other.x) < ACC_CY) && (abs(y - other.y) < ACC_CY);
	}

	bool operator!= (const Point& other) const {
		return !((*this) == other);
	}

	void scale (const Point& p, const double k) {

		double x_x = x - p.x;
		double y_y = y - p.y;

		x = p.x + x_x * k;
		y = p.y + y_y * k;
	}

	void rotate (const Point& center, const double angle);
	bool In (Point first, Point second) const;
};

Point operator- (const Point& first, const Point& second) {
	return Point(first.x - second.x, first.y - second.y);
}

Point operator+ (const Point& first, const Point& second) {
	return Point(first.x + second.x, first.y + second.y);
}

// scalar
double operator* (const Point& first, const Point& second) {		// point as vector	
	return (first.x * second.x + first.y * second.y);
}

// module of vector
double operator^ (const Point& first, const Point& second) {		// point as vector	
	return (first.x * second.y - first.y * second.x);
}

double abs (const Point& vec) {
	return pow(vec.x * vec.x + vec.y * vec.y, 0.5);
}

void Point::rotate (const Point& center, const double angle) {

	Point point = (*this) - center;
	double new_x = point.x * cos (angle) - point.y * sin (angle);
	double new_y = point.x * sin (angle) + point.y * cos (angle);

	point.x = new_x;
	point.y = new_y;

	point = point + center;
	(*this) = point;
}

//----------------------------------------------------------------------//
//----------------------------------------------------------------------//

class Line {

public:
	// y = kx + d;
	// ax + by + c = 0;

	Line () {};
	~Line () {};

	Line (Point first, Point second) {

		if (abs(second.x - first.x) < ACC_CY) { // x - c = 0;
			b = 0.0;
			c = -first.x;
			a = 1.0;
			return;
		}

		if (abs(second.y - first.y) < ACC_CY) { // y - c = 0;
			b = 1.0;
			c = -first.y;
			a = 0.0;
			return;
		}

		a = first.y - second.y;
		b = second.x - first.x;
		//c = first.x * (second.y - first.y) - first.y * (second.x - first.x);
		c = first.x * second.y - first.y * second.x;
	}

	Line (Point v, double k) {

		c = - k * v.x + v.y;
		a = k;
		b = -1;
	}

	Line (double k, double d) {

		a = k;
		c = d;
		b = -1;
	}

	Line (double a, double b, double c) : a(a), b(b), c(c) {}

	void Normalize () {

		// x + by + c = 0 or y + c = 0 or x + c = 0
		if (abs(a) < ACC_CY) {

			if (abs(b) < ACC_CY) {
				cout << "ERROR : WRONG LINE EQUATION\n";
			}

			c /= b;
			b = 1.0; 
		} else {

			b /= a;
			c /= a;
			a = 1.0;
		}
	}

	bool operator== (const Line& other) const {

		Line first = (*this);
		Line second = other;

		first.Normalize ();
		second.Normalize ();

		return ((abs(first.a - second.a) < ACC_CY) && 
				(abs(first.b - second.b) < ACC_CY) && 
				(abs(first.c - second.c) < ACC_CY));

	}

	bool operator!= (const Line& other) const {
		return !((*this) == other);
	}

	Line perpendicular (const Point& p) const {

		Line result (b, -a, a * p.y - b * p.x);
		return result;
	}

	bool upper (const Point& p) const {				// line is upper than point
		return (a * p.x + b * p.y + c < ACC_CY);
	}

	bool lower (const Point& p) const {
		return (a * p.x + b * p.y + c > ACC_CY);
	}

	Point InDistance (Point from_point, double dist, string up_down) {

		Point result;

		if (abs(a) < ACC_CY) {
			result.y = from_point.y;
			result.x = ((up_down == "up") ? (from_point.x + dist) : (from_point.x - dist));

			return result;
		}
		
		double equation_a = (b * b) + (a * a);
		double equation_b = 2 * (c * b + a * b * from_point.x - a * a * from_point.y);
		double equation_c = - pow (dist * a, 2) + (c * c) + pow ((from_point.x * a), 2) + 
							2 * c * a * from_point.x  + pow ((from_point.y * a), 2);
		
		pair <double, double> my_y = SolveEquation (equation_a, equation_b, equation_c); // first +, second -

		result.y = ((up_down == "up") ? my_y.first : my_y.second);
		result.x = (- c - b * result.y) / a;

		return result;
	}

	bool containsPoint (Point point) const {

		return (abs(a * point.x + b * point.y + c) < ACC_CY);
	}

	friend Point crossing (const Line& l1, const Line& l2);

private:

	double a = 0.0;
	double b = 0.0;
	double c = 0.0;
};

Point crossing (const Line& l1, const Line& l2) {

	if (abs(l2.b * l1.a - l1.b * l2.a) < ACC_CY) {
		cout << "parallel\n";
	}

	double new_y = (l2.a * l1.c - l2.c * l1.a) / (l2.b * l1.a - l1.b * l2.a);
	double new_x = 0.0;

	if (abs(l1.a) > ACC_CY) {
		new_x = (-l1.c - l1.b * new_y) / l1.a;

	} else if (abs(l2.a) > ACC_CY) {
		new_x = (-l2.c - l2.b * new_y) / l2.a;

	} else {
		cout << "parallel\n";
	}

	return Point (new_x, new_y);
}

Point Reflex (const Point& p, const Line& l) {

	Line perp = l.perpendicular (p);
	Point cross = crossing (perp, l);

	double new_x = cross.x + (cross.x - p.x);
	double new_y = cross.y + (cross.y - p.y);
	
	Point new_p(new_x, new_y);
	return new_p;
}

Point Division (Point first, Point second, double coeff) {

	// AB - M ; coeff = AM / MB; AM < BM;

	double new_x = (first.x + second.x * coeff) / (coeff + 1); // TO CHECK
	double new_y = (first.y + second.y * coeff) / (coeff + 1); // TO CHECK

	return Point (new_x, new_y);
}

bool Point::In (Point first, Point second) const {

	Line my_line(first, second);
	if (!my_line.containsPoint((*this))) {
		return false;
	}

	if (first.x >= second.x + ACC_CY) { // swap
		Point t = first;
		first = second;
		second = t;
	}

	if ((x >= second.x - ACC_CY) || (x <= first.x + ACC_CY)) {
		return false;
	}

	if ((y <= second.y + ACC_CY) && (y >= first.y - ACC_CY)) {
		return true;
	}

	if ((y <= first.y + ACC_CY) && (y >= second.y - ACC_CY)) {
		return true;
	}

	return false;
}

//----------------------------------------------------------------------//
//                               SHAPE                                  //
//----------------------------------------------------------------------//

class Shape {

public:

	Shape () {};
	virtual ~Shape () {};

	virtual double perimeter () const = 0;
	virtual double area () const = 0;

	virtual bool operator== (const Shape& other) const = 0;
	virtual bool operator!= (const Shape& other) const = 0;

	virtual bool isCongruentTo (const Shape& other) const = 0;
	virtual bool isSimilarTo (const Shape& other) const = 0;
	virtual bool containsPoint (Point point) const = 0;

	virtual void rotate (Point center, double angle) = 0;
	virtual void reflex (Point center) = 0;
	virtual void reflex (Line axis) = 0;
	virtual void scale (Point center, double coefficient) = 0;

};

//----------------------------------------------------------------------//
//                              POLYGON                                 //
//----------------------------------------------------------------------//

class Polygon : public Shape {

public:

	Polygon () {};
	~Polygon () {};

	Polygon (vector<Point> points) : vertices(points) {}

	void Add () {};

	template<typename Head, typename... Tail>
	void Add (const Head& head, const Tail& ...tail) {
		vertices.push_back(head);
		Add (tail...);
	} 

	template<typename... Args> 
	Polygon (Args... args) {
		Add(args...);
	}

	int verticesCount () const {

		return vertices.size();
	}

	vector<Point> getVertices () const {

		return vertices;
	}

	bool isConvex () const {


		int size = vertices.size();

		int sign = 0;

		for (int i = 0; i < size; i++) {

			Point left  = vertices[(i - 1 + size) % size];
			Point right = vertices[(i + 1) % size];

			Point now = vertices[i];

			double scale = (left - now) ^ (right - now);

			if (sign == 0) {

				if (scale > 0) {
					sign = 1;
				} else {
					sign = -1;
				}
				continue;
			}

			if ((scale > 0 && sign < 0) || (scale < 0 && sign > 0)) {
				return false;
			}
		}

		return true;
	}

	double perimeter () const override;
	double area () const override;

	bool operator== (const Shape& other) const override;
	bool operator!= (const Shape& other) const override;

	bool isCongruentTo (const Shape& other) const override;
	bool isSimilarTo (const Shape& other) const override;
	bool containsPoint (Point point) const override;

	void rotate (Point center, double angle) override;
	void reflex (Point center) override;
	void reflex (Line axis) override;
	void scale (Point center, double coefficient) override;

protected:

	vector<Point> vertices;
};

double Polygon::perimeter () const {

	double result = 0.0;
	int size = vertices.size();

	for (int i = 0; i < size; i++) {

		Point right = vertices[(i + 1) % size];
		Point now = vertices[i];

		result += abs(right - now);
	}

	return result;
}

double Polygon::area () const {

	double result = 0.0;
	int size = vertices.size();

	Point point = vertices[0];

	for (int i = 0; i < size; i++) {

		Point now  = vertices[i];
		Point right = vertices[(i + 1) % size];

		result += (now - point) ^ (right - point);
	}

	result = abs(result) / 2;

	return result;
}

bool Polygon::operator== (const Shape& other) const {

	Polygon first  = (*this);

	try {
		Polygon second = dynamic_cast<const Polygon&> (other); 

		if (second.verticesCount () != first.verticesCount ()) {
			return false;
		}

		vector<Point> other_vert = second.getVertices ();
		size_t start_ind = 0;
		
		while (other_vert[start_ind++] != first.vertices[0]) {

			if (start_ind == other_vert.size()) {
				return false;
			}
		};

		start_ind--;

		bool first_side = true;

		int size = first.vertices.size();
		for (int i = 0; i < size; i++) {
			if (first.vertices[i] != other_vert[(i + start_ind) % size]) {
				first_side = false;
				break;
			}
		}

		bool second_side = true;

		for (int i = 0; i < size; i++) {
			if (first.vertices[i] != other_vert[(size - i + start_ind) % size]) {
				second_side = false;
				break;
			}
		}

		return (second_side || first_side);
	}

	catch (...) {
		return false;
	}
}

bool Polygon::operator!= (const Shape& other) const {

	return !((*this) == other);
}

bool Polygon::isCongruentTo (const Shape& other) const {

	Polygon first  = (*this);

	try {
		Polygon second = dynamic_cast<const Polygon&> (other); 
		
		if (second.verticesCount () != first.verticesCount ()) {
			return false;
		}

		if (abs(second.area () - first.area ()) >= ACC_CY) {
			return false;
		}

		vector<Point> other_vert = second.getVertices ();
		int size = first.vertices.size();

		bool result  = false;
		bool result_rev = false;

		for (int start_ind = 0; start_ind < size; start_ind ++) {

			result = true;
			for (int i = 0; i < size; i++) {

				Point other_next = other_vert[(i + start_ind + 1) % size];
				Point other_now  = other_vert[(i + start_ind) % size];
				Point other_last = other_vert[(i + start_ind - 1 + size) % size];

				Point next = first.vertices[(i + 1) % size];
				Point now  = first.vertices[i];
				Point last = first.vertices[(i - 1 + size) % size];

				if (abs((other_next - other_now) ^ (other_last - other_now) - 
					(next - now) ^ (last - now)) >= ACC_CY) {
					result = false;
					//break;
				}

				if (abs(other_now - other_next) != abs(now - next)) {
					result = false;
					//break;
				}
			}

			result_rev = true;
			for (int i = 0; i < size; i++) {

				Point other_next = other_vert[(-i + start_ind + 1 + size) % size];
				Point other_now  = other_vert[(-i + start_ind + size) % size];
				Point other_last = other_vert[(-i + start_ind - 1 + 2 * size) % size];

				Point next = first.vertices[(i + 1) % size];
				Point now  = first.vertices[i];
				Point last = first.vertices[(i - 1 + size) % size];

				if (abs((other_next - other_now) ^ (other_last - other_now) - 
					(next - now) ^ (last - now)) >= ACC_CY) {
					result = false;
					//break;
				}

				if (abs(other_now - other_next) != abs(now - next)) {
					result = false;
					//break;
				}
			}

			if (result || result_rev) {
				return true;
			}
		}

		return false;
	}

	catch (...) {
		return false;
	}
}

bool Polygon::isSimilarTo (const Shape& other) const {

	Polygon first  = (*this);

	try {
		Polygon second = dynamic_cast<const Polygon&> (other); 

		if (second.verticesCount () != first.verticesCount ()) {
			return false;
		}

		double sqr_k = (second.area () / first.area ());

		vector<Point> other_vert = second.getVertices ();
		int size = first.vertices.size();

		bool result = false;
		bool result_rev = false;
		
		for (int start_ind = 0; start_ind < size; start_ind ++) {

			result = true;
			for (int i = 0; i < size; i++) {

				Point other_next = other_vert[(i + start_ind + 1) % size];
				Point other_now  = other_vert[(i + start_ind) % size];

				Point next = first.vertices[(i + 1) % size];
				Point now  = first.vertices[i];
				
				if (abs(pow(abs(other_now - other_next) / abs(now - next), 2) - sqr_k) > ACC_CY) {
					result = false;
					break;
				}
			}

			result_rev = true;

			for (int i = 0; i < size; i++) {

				Point other_next = other_vert[(-i + start_ind + 1 + size) % size];
				Point other_now  = other_vert[(-i + start_ind + size) % size];

				Point next = first.vertices[(i + 1) % size];
				Point now  = first.vertices[i];
				
				if (abs(pow(abs(other_now - other_next) / abs(now - next), 2) - sqr_k) > ACC_CY) {
					result_rev = false;
					break;
				}
			}

			if (result || result_rev) {
				return true;
			}
		}

		return false;
	}

	catch (...) {
		return false;
	}
}

bool Polygon::containsPoint (Point point) const {

	double angle = 0;
	int size = vertices.size();

	for (int i = 0; i < size; i++) {

		Point next = vertices[(i + 1) % size];
		Point now = vertices[i];

		if (point == now || point.In(next, now)) {
			return true;
		}

		double vect = (now - point) ^ (next - point);
		double scal = (now - point) * (next - point);
		
		angle += atan2 (vect, scal);	
	}

	return (abs(abs(angle) - 2 * pi) < ACC_CY);
}

void Polygon::rotate (Point center, double angle) {

	angle *= (pi / 180);

	for (int i = 0; i < vertices.size(); i++) {
		vertices[i].rotate (center, angle);
	}
}

void Polygon::reflex (Point center) {

	for (int i = 0; i < vertices.size(); i++) {
		vertices[i].rotate (center, pi);
	}
}

void Polygon::reflex (Line axis) {

	for (int i = 0; i < vertices.size(); i++) {
		vertices[i] = Reflex (vertices[i], axis);
	}
}

void Polygon::scale (Point center, double coefficient) {

	for (int i = 0; i < vertices.size(); i++) {
		vertices[i].scale (center, coefficient);
	}
}

//----------------------------------------------------------------------//
//----------------------------------------------------------------------//

//----------------------------------------------------------------------//
//                              ELLIPSE                                 //
//----------------------------------------------------------------------//

class Ellipse : public Shape {

public:

	~Ellipse () {};
	Ellipse () {};

	Ellipse (Point first, Point second, double d) {

		a = d / 2;
		first_f = first;
		second_f = second;
		b = pow(a * a - pow((abs(first - second) / 2), 2), 0.5); 
	}

	pair<Point, Point> focuses () {
		return {first_f, second_f};
	}

	double eccentricity () {
		double e = pow(1 - b * b /(a * a), 0.5);
		return e;
	}

	pair<Line, Line> directrices () {

		double new_x = a / eccentricity();
		Line first(1, 0, new_x);
		Line second(1, 0, -new_x);

		return {first, second};
	}	

	Point center () {

		double new_x = (second_f.x + first_f.x) / 2;
		double new_y = (second_f.y + first_f.y) / 2;
		
		return Point(new_x, new_y);	 
	} 

	double perimeter () const override;
	double area () const override;

	bool operator== (const Shape& other) const override;
	bool operator!= (const Shape& other) const override;

	bool isCongruentTo (const Shape& other) const override;
	bool isSimilarTo (const Shape& other) const override;
	bool containsPoint (Point point) const override;

	void rotate (Point center, double angle) override;
	void reflex (Point center) override;
	void reflex (Line axis) override;
	void scale (Point center, double coefficient) override;

protected:

	Point first_f;
	Point second_f;

	double a = 0.0;
	double b = 0.0;
};

double Ellipse::perimeter () const {

	// 4(pi * a * b + (a - b)**2) / (a + b) 
	double temp = 3 * pow((a-b)/(a+b), 2);
	double result = pi * (a + b) * (1 + temp / (10 + pow(4 - temp, 0.5)));
	return result;
}

double Ellipse::area () const {
	
	// pi * a * b
	double result = pi * a * b;
	return result;	
}

bool Ellipse::operator== (const Shape& other) const {

	Ellipse this_el  = (*this);
	try {
		Ellipse other_el = dynamic_cast<const Ellipse&> (other);

		bool check_1 = (this_el.first_f  == other_el.second_f) &&
					   (this_el.second_f == other_el.first_f);

		bool check_2 = (this_el.second_f == other_el.second_f) && 
					   (this_el.first_f  == other_el.first_f);

		return ((abs(this_el.a - other_el.a) < ACC_CY) && 
				(abs(this_el.b - other_el.b) < ACC_CY) && (check_2 || check_1));
	}
	catch (...) {
		return false;
	}
}

bool Ellipse::operator!= (const Shape& other) const {

	return !((*this) == other);
}

bool Ellipse::isCongruentTo (const Shape& other) const {

	Ellipse this_el  = (*this);
	try {
		Ellipse other_el = dynamic_cast<const Ellipse&> (other); 
		return (abs(this_el.a - other_el.a) < ACC_CY) && 
			   (abs(this_el.b - other_el.b) < ACC_CY);
		}
	catch (...) {
		return false;
	}
}

bool Ellipse::isSimilarTo (const Shape& other) const {

	Ellipse this_el  = (*this);
	try {

		Ellipse other_el = dynamic_cast<const Ellipse&> (other); 
		double k_1 = this_el.a / other_el.a;
		double k_2 = this_el.b / other_el.b;

		return (abs(k_1 - k_2) < ACC_CY); 							// k_1 == k_2
	}
	catch (...) {
		return false;
	}											
}

bool Ellipse::containsPoint (Point point) const {

	double data = abs(point - first_f) + abs(point - second_f); 						//(x / 2) ** 2 + (y / 2) ** 2 <= 1
	return (data <= 2*a + ACC_CY); 													
}

void Ellipse::rotate (Point center, double angle) {

	first_f. rotate (center, angle);
	second_f.rotate (center, angle);
}

void Ellipse::reflex (Point center) {

	first_f. rotate (center, pi);
	second_f.rotate (center, pi);
}

void Ellipse::reflex (Line axis) {

	first_f  = Reflex (first_f , axis);
	second_f = Reflex (second_f, axis);
}

void Ellipse::scale (Point center, double coefficient) {

	first_f. scale (center, coefficient);
	second_f.scale (center, coefficient);
}

//----------------------------------------------------------------------//
//----------------------------------------------------------------------//

//----------------------------------------------------------------------//
//                               CIRCLE                                 //
//----------------------------------------------------------------------//

class Circle : public Ellipse {

public:

	//Circle () {};
	~Circle () {};
	Circle (Point new_center, double new_r) : Ellipse(), my_center(new_center), r(new_r) {
		first_f = new_center;
		second_f = new_center;
		b = r;
		a = r;
	}

	double radius () {
		return r;
	}

private:

	Point my_center;
	double r = 0.0;
};

//----------------------------------------------------------------------//
//----------------------------------------------------------------------//

//----------------------------------------------------------------------//
//                             RECTANGLE                                //
//----------------------------------------------------------------------//

class Rectangle : public Polygon {

public:

	Rectangle () {};
	~Rectangle () {};

	Rectangle (Point new_a, Point new_c, double coeff) : Polygon() {
		
		if (coeff > 1) {
			coeff = 1 / coeff;
		}

		a = new_a;
		c = new_c;

		Line ac(a, c);
		Point diag = (c - a);
		
		Point h1 = Division (a, c, coeff);
		Line perp = ac.perpendicular (h1);

		//double a_h1 = coeff * abs (a - c) / (coeff + 1);
		double a_h1 = abs (a - h1);
		double b_h1 = a_h1 / coeff;

		string up_down = "up";
		string down_up = "down";

		if (new_a.x > new_c.x) {
			up_down = "down";
			down_up = "up";
		}

		b = perp.InDistance (h1, b_h1, up_down);
		
		Point center((a.x + c.x) / 2, (a.y + c.y) / 2);
		//from center diag / 2 distance 

		Line line_bd(b, center);
		d = line_bd.InDistance (center, abs (b - center), down_up);

		vertices.push_back(a);
		vertices.push_back(b);
		vertices.push_back(c);
		vertices.push_back(d);
	}

	Point center () {

		double new_x = (c.x - a.x) / 2 + a.x;
		double new_y = (c.y - a.y) / 2 + a.y;

		return Point (new_x, new_y);
	}

	pair<Line, Line> diagonals () {

		return {Line (a, c), Line (b, d)};
	}

protected:

	Point a;
	Point b;
	Point c;
	Point d;
};


//----------------------------------------------------------------------//
//----------------------------------------------------------------------//

//----------------------------------------------------------------------//
//                               SQUARE                                 //
//----------------------------------------------------------------------//

class Square : public Rectangle {

public:

	//Square () {};
	//~Square () {};

	Square (Point new_a, Point new_c) : Rectangle() {

		a = new_a;
		c = new_c;
		
		Point center;
		center.x = (a.x + c.x) / 2;
		center.y = (a.y + c.y) / 2;

		Line ac (a, c);
		Line bd = ac.perpendicular (center);

		b = bd.InDistance (center, abs (a - c) / 2, "up");
		d = bd.InDistance (center, abs (a - c) / 2, "down");

		vertices.push_back(a);
		vertices.push_back(b);
		vertices.push_back(c);
		vertices.push_back(d);
	}

	// outside
	Circle circumscribedCircle () {

		return Circle ((*this).center (), abs (c - a) / 2);
	}

	// inside
	Circle inscribedCircle () {

		return Circle ((*this).center (), abs (a - b) / 2);
	}

private:

	Point a;
	Point b;
	Point c;
	Point d;
};

//----------------------------------------------------------------------//
//----------------------------------------------------------------------//

//----------------------------------------------------------------------//
//                             TRIANGLE                                 //
//----------------------------------------------------------------------//

class Triangle : public Polygon {

public:

	Triangle () {};
	~Triangle () {};
	Triangle (Point new_a, Point new_b, Point new_c) : 
				Polygon(new_a, new_b, new_c), a(new_a), b(new_b), c(new_c) {
	}

	Point circumscribedCircleCenter () const {

		double d = 2 * (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y));

		double new_x = ((a.x * a.x + a.y * a.y) * (b.y - c.y) +
						(b.x * b.x + b.y * b.y) * (c.y - a.y) +
						(c.x * c.x + c.y * c.y) * (a.y - b.y)) / d;

		double new_y = ((a.x * a.x + a.y * a.y) * (c.x - b.x) +
						(b.x * b.x + b.y * b.y) * (a.x - c.x) +
						(c.x * c.x + c.y * c.y) * (b.x - a.x)) / d;

		return Point(new_x, new_y);
	}

	Circle circumscribedCircle () const {

		double ab = abs (a - b);
		double ac = abs (a - c);
		double bc = abs (b - c);

		Point center = (*this).circumscribedCircleCenter ();
		double r = ab * bc * ac / (4 * (*this).area());

		return Circle (center, r);
	}

	Circle inscribedCircle () const {

		double s = (*this).area();
		double p = (*this).perimeter() / 2;
		double r = s / p;

		double ab = abs (a - b);
		double ac = abs (a - c);
		double bc = abs (b - c);

		double new_x = (bc * a.x + ac * b.x + ab * c.x) / (ab + ac + bc);
		double new_y = (bc * a.y + ac * b.y + ab * c.y) / (ab + ac + bc);		 

		Point center (new_x, new_y);
		return Circle (center, r);
	}

	Point centroid () const {

		return Point ((a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3);
	}

	Point orthocenter () const {
	
		double det_x  = det (a.y, b.x * c.x + a.y * a.y, 1,
							 b.y, c.x * a.x + b.y * b.y, 1,
							 c.y, a.x * b.x + c.y * c.y, 1);

		double det_xy = det (a.x, a.y, 1, b.x, b.y, 1, c.x, c.y, 1);


		double det_y  = det (b.y * c.y + a.x * a.x, a.x, 1,
							 c.y * a.y + b.x * b.x, b.x, 1,
							 a.y * b.y + c.x * c.x, c.x, 1);	

		return Point (det_x / det_xy, det_y / det_xy);
	}

	Line EulerLine () const {

		return Line ((*this).orthocenter (), (*this).circumscribedCircleCenter ());
	}

	Circle ninePointsCircle () const {

		double ab = abs (a - b);
		double ac = abs (a - c);
		double bc = abs (b - c);

		Point first  = (*this).orthocenter ();
		Point second = (*this).circumscribedCircleCenter ();

		Point center ((first.x + second.x) / 2, (first.y + second.y) / 2);
		Point center_ab ((a.x + b.x) / 2, (a.y + b.y) / 2);
		double r = (ab * bc * ac / (4 * (*this).area())) / 2;

		return Circle (center, r);
	}

private:

	Point a;
	Point b;
	Point c;
};
