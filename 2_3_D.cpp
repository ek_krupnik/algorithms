#include <iostream>
#include <vector>
#include <set>
#include <string>

using namespace std;

class Graph {

public:

	Graph (long long a = 0, long long b = 0, long long n = 0, long long m = 0) : links(n * m), size(n * m), time_one(a), time_two(b), 
		   n(n), m(m), matrix(vector<vector<char>>(n, vector<char>(m))) {}

	void Add (long long from, long long to);
	void AddChar (long long i, long long j, char c);

	long long GetSize () const {return size;}
	vector<long long> GetLinks (long long v) const {return links[v];}
	long long GetIndex (long long i, long long j) const {return m * i + j;}
	void BuildLinks (long long n, long long m);

	bool LeftBroken (long long i, long long j) const;
	bool RightBroken (long long i, long long j) const;
	bool UpBroken (long long i, long long j) const;
	bool DownBroken (long long i, long long j) const;

	long long GetFirstPrice  () const {return time_one;}
	long long GetSecondPrice () const {return time_two;}
	long long GetBroken () const {return cnt_broken;}

private:

	vector<vector<long long>> links;
	vector<vector<char>> matrix;
	long long size = 0;
	long long n = 0;
	long long m = 0;

	long long cnt_broken = 0;

	long long time_one = 0;
	long long time_two = 0;
};

void Graph::Add (long long from, long long to) {
	links[from].push_back(to);
}

void Graph::AddChar (long long i, long long j, char c) {

	matrix[i][j] = c;
}

bool Graph::LeftBroken (long long i, long long j) const {

	if (i == 0) {
		return false;
	}
	return matrix[i - 1][j] == '*';
}

bool Graph::RightBroken (long long i, long long j) const {

	if (i == n - 1) {
		return false;
	}
	return matrix[i + 1][j] == '*';
}

bool Graph::UpBroken (long long i, long long j) const {

	if (j == 0) {
		return false;
	}
	return matrix[i][j - 1] == '*';
}

bool Graph::DownBroken (long long i, long long j) const {

	if (j == m - 1) {
		return false;
	}
	return matrix[i][j + 1] == '*';
}

void Graph::BuildLinks (long long n, long long m) {

	for (long long i = 0; i < n; i++) {
		for (long long j = 0; j < m; j++) {

			if (matrix[i][j] == '.') {
				continue;
			}

			if (LeftBroken(i, j)) {
				Add (GetIndex(i, j), GetIndex(i - 1, j));
			}
			if (RightBroken(i, j)) {
				Add (GetIndex(i, j), GetIndex(i + 1, j));
			}
			if (UpBroken(i, j)) {
				Add (GetIndex(i, j), GetIndex(i, j - 1));
			}
			if (DownBroken(i, j)) {
				Add (GetIndex(i, j), GetIndex(i, j + 1));
			}

			cnt_broken ++;
		}
	}
}

bool FindLengtheningChain (const Graph& graph, long long v, vector<bool> &used, vector<long long> &steam_combination) {

	if (used[v]) {
		return false;
	}

	used[v] = true;

	for (auto to : graph.GetLinks(v)) {
		if (steam_combination[to] == -1 || 
			FindLengtheningChain(graph, steam_combination[to], used, steam_combination)) {
			
			steam_combination[to] = v;
			return true;
		}
	}

	return false;
}

long long FindMintime (const Graph& graph) {

	long long cnt_broken = graph.GetBroken();

	long long answer = 0;
	long long cnt_pair = 0;

	vector<long long> steam_combination (graph.GetSize(), -1);

	for (long long i = 0; i < graph.GetSize(); i++) {
		vector<bool> used(graph.GetSize(), false);
		FindLengtheningChain (graph, i, used, steam_combination);
	}

	for (long long i = 0; i < graph.GetSize(); i++) {
		if (steam_combination[i] != -1) {
			cnt_broken --;
			cnt_pair ++;
		}
	}
	cnt_pair /= 2;

	return min(cnt_broken * graph.GetFirstPrice() + cnt_pair * graph.GetSecondPrice(), 
               (cnt_broken + cnt_pair * 2) * graph.GetFirstPrice());
}

int main() {

	long long n, m, a, b;
	cin >> n >> m >> b >> a;

	Graph graph(a, b, n, m);

	for (long long i = 0; i < n; i++) {
		string s;
		cin >> s;

		for (long long j = 0; j < m; j++) {
			//cout << s[i] << endl;
			graph.AddChar (i, j, s[j]);
		}
	}

	graph.BuildLinks (n, m);
	cout << FindMintime (graph);
}